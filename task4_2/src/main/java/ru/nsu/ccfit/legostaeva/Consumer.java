package ru.nsu.ccfit.legostaeva;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.BlockingQueue;

@Slf4j
@RequiredArgsConstructor
public class Consumer implements Runnable {
    private final BlockingQueue<Resource> sharedQueue;

    @SuppressWarnings("InfiniteLoopStatement")
    @Override
    public void run() {
        while (true) {
            try {
                Resource resource = sharedQueue.take();
                log.info("Thread type: consumer, Resource ID: {}, Consumed", resource.getUuid());
            } catch (InterruptedException e) {
                log.warn("Возникли проблемы при попытке извлечь ресурс из очереди.", e);
            }
        }
    }
}
