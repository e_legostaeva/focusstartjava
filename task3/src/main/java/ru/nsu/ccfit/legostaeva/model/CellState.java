package ru.nsu.ccfit.legostaeva.model;

/**
 * Представление, хранящее состояния ячейки.
 */
public enum CellState {
    OPENED {
        @Override
        String getCellType(Cell cell) {
            if (cell.isMined()) {
                return "bombed";
            }
            return "num" + cell.getNumberOfMinesAround();
        }

        @Override
        String getCellTypeAfterLose(Cell cell) {
            return getCellType(cell);
        }
    },
    CLOSED {
        @Override
        String getCellType(Cell cell) {
            return "closed";
        }

        @Override
        String getCellTypeAfterLose(Cell cell) {
            if (cell.isMined()) {
                return "bomb";
            }
            return "closed";
        }
    },
    FLAGGED {
        @Override
        String getCellType(Cell cell) {
            return "flagged";
        }

        @Override
        String getCellTypeAfterLose(Cell cell) {
            if (cell.isMined()) {
                return "flagged";
            }
            return "nobomb";
        }
    },
    QUESTION {
        @Override
        String getCellType(Cell cell) {
            return "question";
        }

        @Override
        String getCellTypeAfterLose(Cell cell) {
            return "question";
        }
    };

    abstract String getCellType(Cell cell);

    abstract String getCellTypeAfterLose(Cell cell);
}
