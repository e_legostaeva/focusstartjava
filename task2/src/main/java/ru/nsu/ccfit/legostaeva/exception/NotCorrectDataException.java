package ru.nsu.ccfit.legostaeva.exception;

/**
 * Исключение, связанное с некорректными входными данными.
 */
public class NotCorrectDataException extends Exception {
    public NotCorrectDataException(String message) {
        super(message);
    }
}
