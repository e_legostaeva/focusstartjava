package ru.nsu.ccfit.legostaeva.file;

import lombok.extern.slf4j.Slf4j;
import ru.nsu.ccfit.legostaeva.exception.NotCorrectDataException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.net.URL;
import java.util.Optional;

@Slf4j
public class FileProvider {
    private static final String DEFAULT_FILE_NAME = "input.txt";

    /**
     * Получает имя входного файла. Если такой файл существует, возвращает его, если нет, возращает файл по умолчанию.
     *
     * @param inputFileName имя входного файла.
     * @return входной файл.
     * @throws NotCorrectDataException если при работе с файлом по умолчанию произошла одна из следующих ошибок:
     *                                 <li>Нет прав на чтение файла;
     *                                 <li>Файл не существует.
     */
    @Nonnull
    public File getInputFile(@Nullable String inputFileName) throws NotCorrectDataException {
        if (inputFileName == null) {
            return getDefaultInputFile();
        }

        File inputFile = new File(inputFileName);
        if (!fileIsValid(inputFile)) {
            log.warn("Файл " + inputFile.getName() + " не существует, либо нет прав на чтение.");
            return getDefaultInputFile();
        } else {
            return inputFile;
        }
    }

    /**
     * Получает файл вывода по его имени.
     *
     * @param outputFileName имя файла вывода.
     * @return файл вывода или null, если {@code outputFileName == null}.
     * @throws NotCorrectDataException нет прав на запись в файл.
     */
    @Nullable
    public File getOutputFile(@Nullable String outputFileName) throws NotCorrectDataException {
        if (outputFileName == null) {
            return null;
        }

        File outputFile = new File(outputFileName);
        if (!outputFile.canWrite()) {
            throw new NotCorrectDataException(String.format("Нет прав на запись в файл %s.", outputFileName));
        }

        log.debug("Используемый файл вывода: {}", outputFileName);
        return outputFile;
    }

    @Nonnull
    private File getDefaultInputFile() throws NotCorrectDataException {
        URL fileUrl = getClass().getClassLoader().getResource(DEFAULT_FILE_NAME);
        File defaultInputFile = Optional.ofNullable(fileUrl)
                .map(URL::getFile)
                .map(File::new)
                .orElseThrow(() -> new NotCorrectDataException("Не найден файл " + DEFAULT_FILE_NAME));

        if (!fileIsValid(defaultInputFile)) {
            throw new NotCorrectDataException("Нет корректных данных в " + defaultInputFile.getName());
        } else {
            log.debug("Для работы взят файл по умолчанию.");
            return defaultInputFile;
        }
    }

    private boolean fileIsValid(File file) {
        return (file.exists()) && (file.canRead());
    }
}
