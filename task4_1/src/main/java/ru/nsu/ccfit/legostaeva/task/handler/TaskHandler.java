package ru.nsu.ccfit.legostaeva.task.handler;

import lombok.ToString;
import ru.nsu.ccfit.legostaeva.task.Task;

@ToString(callSuper = true)
public class TaskHandler extends AbstractTaskHandler {

    public TaskHandler(Task task) {
        super(task);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double process(int curNumber) {
        return 2 * Math.pow(Math.sin(Math.PI * curNumber / 3), 2) / Math.pow(3, curNumber);
    }
}
