package ru.nsu.ccfit.legostaeva;

import com.google.common.collect.ImmutableList;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.nsu.ccfit.legostaeva.task.Task;
import ru.nsu.ccfit.legostaeva.task.WorkQueue;
import ru.nsu.ccfit.legostaeva.task.handler.TaskHandler;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Тесты на пул тредов")
class WorkQueueTest {
    private static final int THREADS_NUMBER = Runtime.getRuntime().availableProcessors();
    private List<Task> tasks = ImmutableList.of(
            new Task(1, 101),
            new Task(101, 201),
            new Task(201, 301),
            new Task(301, 401),
            new Task(401, 501),
            new Task(501, 601),
            new Task(601, 701)
    );

    @Test
    @DisplayName("Проверка значения суммы ряда")
    void execute_returnSumOfTheSeries_ifAvailableProcessorsPassed() {
        WorkQueue workQueue = new WorkQueue(THREADS_NUMBER);
        tasks.stream()
                .map(TaskHandler::new)
                .forEach(workQueue::execute);
        workQueue.terminateExecutionOfAllThreads();

        double result = tasks.stream()
                .map(Task::getResult)
                .reduce(0D, (result1, result2) -> result1 + result2);

        assertEquals(0.6923076923076922, result);
    }
}