package ru.nsu.ccfit.legostaeva.evaluator;

public class MultiplicationEvaluator extends AbstractEvaluator {
    /**
     * {@inheritDoc}
     */
    @Override
    int performOperation(int a, int b) {
        return a * b;
    }
}