package ru.nsu.ccfit.legostaeva.view;

import lombok.extern.slf4j.Slf4j;
import ru.nsu.ccfit.legostaeva.controller.MinesweeperController;
import ru.nsu.ccfit.legostaeva.level.GameLevel;
import ru.nsu.ccfit.legostaeva.level.GameLevelUtils;
import ru.nsu.ccfit.legostaeva.view.menuBar.MenuBar;

import javax.swing.*;
import java.awt.*;

/**
 * {@inheritDoc}
 */
@Slf4j
public class SwingMinesweeperView extends JFrame implements MinesweeperView {
    private final MinesweeperController controller;

    private JMenuBar menuBar;
    private GameLevel gameLevel;
    private ViewField field;
    private GameInformationPanel gameInformationPanel;

    public SwingMinesweeperView(MinesweeperController minesweeperController) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.controller = minesweeperController;
        this.gameLevel = GameLevel.BEGINNER;
        this.field = new ViewField(GameLevelUtils.getLevelParametersByLevel(gameLevel).getRowNumber(),
                GameLevelUtils.getLevelParametersByLevel(gameLevel).getColumnNumber(),
                controller);
        init();
        controller.maxFieldSizeHandle(getWindowHeightWithoutComponents(screenSize.height) / ViewField.getImageHeight(),
                screenSize.width / ViewField.getImageWidth()
        );
    }

    @Override
    public void renderNewGame(int rowNumber, int columnNumber, int minesNumber) {
        field.createNewField(rowNumber, columnNumber);
        gameInformationPanel.changeMinesCounter(minesNumber, minesNumber);
        gameInformationPanel.changeTimer(0);
        pack();
    }

    @Override
    public void updateCell(int row, int column, String cellType) {
        field.updateCell(row, column, cellType);
    }

    @Override
    public void renderGameLost(String[][] fieldTypes) {
        for (int i = 0; i < fieldTypes.length; ++i) {
            for (int j = 0; j < fieldTypes[0].length; ++j) {
                this.field.updateCell(i, j, fieldTypes[i][j]);
            }
        }
        showRequestForNewGame();
    }

    @Override
    public void renderGameWon(GameLevel gameLevel) {
        if (gameLevel != GameLevel.SPECIAL) {
            String name = "";

            while ("".equals(name)) {
                name = JOptionPane.showInputDialog(this,
                        "Введите свое имя",
                        "Победа!",
                        JOptionPane.INFORMATION_MESSAGE
                );
            }

            if (name != null) {
                controller.highScoreNewValueHandle(name);
            }
        }
        showRequestForNewGame();
    }

    @Override
    public void updateMinesCounter(int flaggedCellNumber, int minesNumber) {
        gameInformationPanel.changeMinesCounter(minesNumber - flaggedCellNumber, minesNumber);
    }

    @Override
    public void updateTimer(int timeSeconds) {
        gameInformationPanel.changeTimer(timeSeconds);
    }

    @Override
    public void renderFailWhileAddingNewScore() {
        showErrorDialog("К сожалению, ваш результат не будет записан.");
    }

    @Override
    public void renderFailWhileLoadingScores() {
        showErrorDialog("К сожалению, невозможно загрузить таблицу рекордов.");
    }

    @Override
    public void showHighScores(String currentScores) {
        JOptionPane.showMessageDialog(this, currentScores);
    }

    @Override
    public void renderTooBigGameParameters(int maxRows, int maxColumns, int maxMinesAmount) {
        showErrorDialog(createTooBigGameParametersMessage(maxRows, maxColumns, maxMinesAmount));
    }

    @Override
    public void renderLittleGameParameters(int minRows, int minColumns, int minMinesAmount) {
        showErrorDialog(createLittleGameParametersMessage(minRows, minColumns, minMinesAmount));
    }

    @Override
    public void renderTooBigMinesAmount(int maxMinesAmount) {
        showErrorDialog(createTooBigMinesAmountMessage(maxMinesAmount));
    }

    private void initFrame() {
        pack();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Сапер");
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
        setIconImage(IconFactory.getInstance().getImageIcon("icon").getImage());
    }

    private void initPanel() {
        gameInformationPanel = new GameInformationPanel(GameLevelUtils.getLevelParametersByLevel(gameLevel).getMinesNumber(),
                GameLevelUtils.getLevelParametersByLevel(gameLevel).getMinesNumber(), 0);
        add(gameInformationPanel, BorderLayout.NORTH);
        JPanel gamePanel = new JPanel();
        gamePanel.add(field.getField());
        add(gamePanel, BorderLayout.CENTER);
    }

    private void init() {
        setLayout(new BorderLayout());
        initMenu();
        initPanel();
        initFrame();
    }

    private void initMenu() {
        menuBar = (new MenuBar(controller, this)).getMenuBar();
        setJMenuBar(menuBar);
    }

    public GameLevel getGameLevel() {
        return gameLevel;
    }

    private void endOfGame(int chosenOption) {
        if (chosenOption == JOptionPane.YES_OPTION) {
            controller.startNewGameHandle();
        }
    }

    private int getWindowHeightWithoutComponents(int screenHeight) {
        return screenHeight - gameInformationPanel.getHeight() - menuBar.getHeight() - this.getInsets().top;
    }

    private String createTooBigGameParametersMessage(int maxRows, int maxColumns, int maxMinesAmount) {
        return "Слишком большие параметры." +
                System.lineSeparator() +
                "Максимальные значения, подходящие для вашего экрана:" +
                System.lineSeparator() +
                "Ширина: " + maxRows +
                System.lineSeparator() +
                "Высота: " + maxColumns +
                System.lineSeparator() +
                "Число мин: " + maxMinesAmount;
    }


    private String createTooBigMinesAmountMessage(int maxMinesAmount) {
        return "Слишком большое число мин." +
                System.lineSeparator() +
                "Максимальное число мин, подходящее для заданного поля:" + maxMinesAmount;
    }

    private String createLittleGameParametersMessage(int minRows, int minColumns, int minMinesAmount) {
        return "Слишком маленькие параметры." +
                System.lineSeparator() +
                "Минимальные возможные значения:" +
                System.lineSeparator() +
                "Ширина: " + minRows +
                System.lineSeparator() +
                "Высота: " + minColumns +
                System.lineSeparator() +
                "Число мин: " + minMinesAmount;
    }

    private void showRequestForNewGame() {
        int gameWonDialogResult = JOptionPane.showConfirmDialog(
                this,
                "Хотите сыграть еще раз?",
                "Игра окончена",
                JOptionPane.YES_NO_OPTION);
        endOfGame(gameWonDialogResult);
    }

    private void showErrorDialog(String message) {
        JOptionPane.showMessageDialog(this,
                message,
                "Ошибка",
                JOptionPane.ERROR_MESSAGE
        );
    }
}
