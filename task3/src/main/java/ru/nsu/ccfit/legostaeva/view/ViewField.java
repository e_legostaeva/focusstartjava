package ru.nsu.ccfit.legostaeva.view;

import lombok.Getter;
import ru.nsu.ccfit.legostaeva.controller.MinesweeperController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Игровое поле.
 */
@Getter
class ViewField {
    private static final int IMAGE_HEIGHT = 50;
    private static final int IMAGE_WIDTH = 50;
    private final MinesweeperController controller;
    private final JPanel field;
    private final IconFactory imageIconFactory;
    private ViewCell[][] cells;

    ViewField(int rowNumber, int columnNumber, MinesweeperController controller) {
        this.controller = controller;
        this.field = new JPanel();
        this.imageIconFactory = IconFactory.getInstance();

        initField(rowNumber, columnNumber);
    }

    private void initField(int rowNumber, int columnNumber) {
        field.setLayout(new GridLayout(rowNumber, columnNumber));
        this.cells = new ViewCell[rowNumber][columnNumber];

        for (int i = 0; i < rowNumber; ++i) {
            for (int j = 0; j < columnNumber; ++j) {
                ViewCell cell = new ViewCell(i, j);
                cell.setIcon(imageIconFactory.getImageIcon("closed"));
                cell.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent e) {
                        if (SwingUtilities.isLeftMouseButton(e)) {
                            controller.leftClickHandle(cell.getRow(), cell.getColumn());
                        }
                        if (SwingUtilities.isRightMouseButton(e)) {
                            controller.rightClickHandle(cell.getRow(), cell.getColumn());
                        }
                        if (SwingUtilities.isMiddleMouseButton(e)) {
                            controller.middleClickHandle(cell.getRow(), cell.getColumn());
                        }
                    }
                });
                cells[i][j] = cell;
                field.add(cell);
            }
        }
    }

    void updateCell(int row, int column, String cellType) {
        cells[row][column].setIcon(imageIconFactory.getImageIcon(cellType));
    }

    void createNewField(int row, int column) {
        field.removeAll();
        initField(row, column);
    }

    static int getImageHeight() {
        return IMAGE_HEIGHT;
    }

    static int getImageWidth() {
        return IMAGE_WIDTH;
    }
}
