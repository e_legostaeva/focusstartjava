package ru.nsu.ccfit.legostaeva;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Getter
@RequiredArgsConstructor
class Resource {
    private final UUID uuid;
}
