package ru.nsu.ccfit.legostaeva.printer;

public interface Printer {
    /**
     * Выполняет форматирование заданной таблицы, а также выводит ее на экран
     *
     * @param table таблицы, которую нужно вывести
     */
    void printFormatted(int[][] table);
}