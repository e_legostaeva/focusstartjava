package ru.nsu.ccfit.legostaeva.figure.supplier;

import ru.nsu.ccfit.legostaeva.figure.implementations.Circle;
import ru.nsu.ccfit.legostaeva.exception.NotCorrectDataException;

import java.math.BigDecimal;
import java.util.List;

/**
 * {@inheritDoc}
 */
public class CircleSupplier implements FigureSupplier<Circle> {
    @Override
    public Circle getFigure(List<BigDecimal> figureParameters) throws NotCorrectDataException {
        return new Circle(figureParameters);
    }
}
