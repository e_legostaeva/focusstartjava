package ru.nsu.ccfit.legostaeva.view;

import javax.swing.*;
import java.awt.*;

/**
 * Информационная панель. Содержит оставщееся количество мин и время, прошедшее с начала игры.
 */
class GameInformationPanel extends JPanel {
    private static final String TIME = "Время: ";
    private static final String MINES = "Мины: ";
    private static final String MINES_SEPARATOR = "/";

    private JLabel minesCounter;
    private JLabel timer;

    GameInformationPanel(int currentMinesNumber, int minesNumber, int timeSeconds) {
        setBorder(BorderFactory.createEtchedBorder());
        setLayout(new FlowLayout(FlowLayout.LEFT));

        minesCounter = new JLabel();
        minesCounter.setText(MINES + currentMinesNumber + MINES_SEPARATOR + minesNumber);
        add(minesCounter);

        timer = new JLabel();
        timer.setText(TIME + timeSeconds);
        add(timer);
    }

    void changeMinesCounter(int currentMinesNumber, int minesNumber) {
        minesCounter.setText(MINES + currentMinesNumber + MINES_SEPARATOR + minesNumber);
    }

    void changeTimer(int timeSeconds) {
        timer.setText(TIME + timeSeconds);
    }

}
