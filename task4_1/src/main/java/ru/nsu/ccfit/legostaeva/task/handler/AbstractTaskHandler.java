package ru.nsu.ccfit.legostaeva.task.handler;

import lombok.RequiredArgsConstructor;
import lombok.ToString;
import ru.nsu.ccfit.legostaeva.task.Task;

@ToString
@RequiredArgsConstructor
public abstract class AbstractTaskHandler implements Runnable {
    private final Task task;

    @Override
    public void run() {
        double result = 0;
        for (int i = task.getLowerBoundOfDataInclusive(); i < task.getUpperBoundOfDataExclusive(); ++i) {
            result += process(i);
        }
        task.setResult(result);
    }

    /**
     * Считает значение функции.
     *
     * @param curNumber неизвествное, подставляемое в функцию.
     * @return посчитанное значение.
     */
    protected abstract double process(int curNumber);
}
