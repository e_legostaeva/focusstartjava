package ru.nsu.ccfit.legostaeva.validator;

public class UpperLimitValidator implements Validator<Integer> {
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isValid(Integer upperLimit) {
        return upperLimit >= 1;
    }
}
