package ru.nsu.ccfit.legostaeva.extractor;

import ru.nsu.ccfit.legostaeva.validator.UpperLimitValidator;
import ru.nsu.ccfit.legostaeva.validator.Validator;

import javax.annotation.Nullable;
import java.io.InputStream;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class UpperExtractorImpl implements UpperLimitExtractor{
    private final Validator<Integer> validator = new UpperLimitValidator();

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public Integer getUpperLimitFromArguments(String[] args) {
        if (args.length == 1) {
            try {
                int upperLimit = Integer.parseInt(args[0]);
                if (validator.isValid(upperLimit)) {
                    return upperLimit;
                } else {
                    return null;
                }
            } catch (NumberFormatException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getUpperLimitFromInputStream(InputStream inputStream) {
        int upperLimit = 0;
        Scanner in = new Scanner(inputStream);
        do {
            System.out.println("Enter the upper limit value, please.");
            try {
                upperLimit = in.nextInt();
            } catch (NoSuchElementException e) {
                in.next();
            }
        } while (!validator.isValid(upperLimit));
        return upperLimit;
    }
}
