package ru.nsu.ccfit.legostaeva.view.menuBar;

import lombok.Getter;
import ru.nsu.ccfit.legostaeva.controller.MinesweeperController;
import ru.nsu.ccfit.legostaeva.level.GameLevel;
import ru.nsu.ccfit.legostaeva.view.SwingMinesweeperView;

import javax.swing.*;

/**
 * Панель меню.
 */
@Getter
public class MenuBar {
    private final SwingMinesweeperView owner;
    private final MinesweeperController controller;
    private final JMenuBar menuBar;

    public MenuBar(MinesweeperController controller, SwingMinesweeperView owner) {
        this.controller = controller;
        this.owner = owner;
        this.menuBar = new JMenuBar();
        initMenuBar();
    }

    private void initMenuBar() {
        JMenu gameMenu = new JMenu("Игра");
        menuBar.add(gameMenu);

        JMenuItem newGame = new JMenuItem("Начать заново");
        gameMenu.add(newGame);
        newGame.addActionListener(e -> controller.restartNewGameHandle());

        JMenuItem settings = new JMenuItem("Настройки");
        gameMenu.add(settings);
        settings.addActionListener(e -> new SettingsDialog(owner, owner.getGameLevel(), controller));

        JMenuItem exit = new JMenuItem("Выход");
        gameMenu.add(exit);
        exit.addActionListener(e -> System.exit(0));

        JMenu highScores = new JMenu("Рекорды");
        menuBar.add(highScores);

        JMenuItem beginner = new JMenuItem("Новичок");
        highScores.add(beginner);
        beginner.addActionListener(e -> controller.showHighScoresHandle(GameLevel.BEGINNER));

        JMenuItem intermediate = new JMenuItem("Средний");
        highScores.add(intermediate);
        intermediate.addActionListener(e -> controller.showHighScoresHandle(GameLevel.INTERMEDIATE));

        JMenuItem expert = new JMenuItem("Профи");
        highScores.add(expert);
        expert.addActionListener(e -> controller.showHighScoresHandle(GameLevel.EXPERT));
    }
}
