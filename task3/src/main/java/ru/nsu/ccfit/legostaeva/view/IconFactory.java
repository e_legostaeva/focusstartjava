package ru.nsu.ccfit.legostaeva.view;

import javax.swing.*;


class IconFactory {
    private static final IconFactory ourInstance = new IconFactory();
    private static final String IMAGES_PACKAGE = "/img/";
    private static final String IMAGES_EXTENSION = ".png";

    static IconFactory getInstance() {
        return ourInstance;
    }

    /**
     * Поставляет нужную картинку.
     *
     * @param name название нужной картинки без расширения.
     * @return картинку, название которой было передано.
     */
    ImageIcon getImageIcon(String name) {
        String filename = IMAGES_PACKAGE + name.toLowerCase() + IMAGES_EXTENSION;
        return new ImageIcon(getClass().getResource(filename));
    }
}
