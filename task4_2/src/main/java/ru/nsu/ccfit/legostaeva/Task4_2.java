package ru.nsu.ccfit.legostaeva;

import lombok.extern.slf4j.Slf4j;
import ru.nsu.ccfit.legostaeva.exception.NotCorrectDataException;
import ru.nsu.ccfit.legostaeva.file.FileProvider;
import ru.nsu.ccfit.legostaeva.valueloader.PropertiesValueLoader;
import ru.nsu.ccfit.legostaeva.valueloader.PropertiesValueLoaderResult;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.*;

@Slf4j
public class Task4_2 {
    public static void main(String[] args) {
        String fileName = null;
        if (args != null && args.length > 0) {
            fileName = args[0];
        }

        File inputFile;
        FileProvider fileProvider = new FileProvider();

        try {
            inputFile = fileProvider.getInputFile(fileName);
        } catch (NotCorrectDataException e) {
            log.error("Входной файл и файл по умолчанию не существуют, либо нет прав на чтение.", e);
            return;
        }

        PropertiesValueLoader propertiesValueLoader = new PropertiesValueLoader();
        PropertiesValueLoaderResult propertiesValueLoaderResult;

        try (InputStream inputStream = new FileInputStream(inputFile)) {
            propertiesValueLoaderResult = propertiesValueLoader.loadPropertiesValues(inputStream);
        } catch (NotCorrectDataException | IOException e) {
            log.error("Возникли проблемы при чтении из .properties", e);
            return;
        }

        BlockingQueue<Resource> sharedQueue = new LinkedBlockingQueue<>(propertiesValueLoaderResult.getStorageSize());

        ScheduledExecutorService producersExecutor = Executors.newScheduledThreadPool(propertiesValueLoaderResult.getProducerThreadsNumber());
        ScheduledExecutorService consumersExecutor = Executors.newScheduledThreadPool(propertiesValueLoaderResult.getConsumerThreadsNumber());

        for (int i = 0; i < propertiesValueLoaderResult.getProducerThreadsNumber(); ++i) {
            Producer producer = new Producer(sharedQueue);
            producersExecutor.scheduleAtFixedRate(producer, 0, propertiesValueLoaderResult.getTimeForProducerThreads(), TimeUnit.MILLISECONDS);
        }

        for (int i = 0; i < propertiesValueLoaderResult.getConsumerThreadsNumber(); ++i) {
            Consumer consumer = new Consumer(sharedQueue);
            consumersExecutor.scheduleAtFixedRate(consumer, 0, propertiesValueLoaderResult.getTimeForConsumerThreads(), TimeUnit.MILLISECONDS);
        }
    }
}
