package ru.nsu.ccfit.legostaeva.view.menuBar;

import lombok.extern.slf4j.Slf4j;
import ru.nsu.ccfit.legostaeva.controller.MinesweeperController;
import ru.nsu.ccfit.legostaeva.level.GameLevel;
import ru.nsu.ccfit.legostaeva.level.GameLevelUtils;
import ru.nsu.ccfit.legostaeva.level.LevelParameter;

import javax.swing.*;
import java.awt.*;
import java.util.Optional;

/**
 * Класс, отвечающий за создание новой игры с заданными пользователем параметрами.
 */
@Slf4j
class SettingsDialog extends JDialog {
    private static final int TEXT_FIELD_SIZE = 10;
    private static final int GRID_COLUMNS = 2;
    private static final int SPRING_PREFERRED_INITIAL_X = 6;
    private static final int SPRING_PREFERRED_INITIAL_Y = 6;
    private static final int SPRING_PREFERRED_X = 6;
    private static final int SPRING_PREFERRED_Y = 6;
    private static final int GRID_LAYOUT_ROWS = 4;
    private static final int GRID_LAYOUT_COLUMNS = 1;
    private static final String ROWS_STRING = "Строки: ";
    private static final String COLUMNS_STRING = "Столбцы: ";
    private static final String MINES_STRING = "Мины: ";

    private GameLevel gameLevel;
    private JTextField height;
    private JTextField width;
    private JTextField mines;


    SettingsDialog(Frame owner, GameLevel gameLevel, MinesweeperController controller) {
        super(owner);
        setTitle("Настройки");
        setLayout(new BorderLayout());
        JPanel settingsPanel = new JPanel();
        this.gameLevel = gameLevel;

        JRadioButton beginner = new JRadioButton("НОВИЧОК");
        JRadioButton intermediate = new JRadioButton("СРЕДНИЙ");
        JRadioButton expert = new JRadioButton("ПРОФИ");
        JRadioButton special = new JRadioButton("ПРОИЗВОЛЬНЫЙ");

        ButtonGroup settingsGroup = new ButtonGroup();
        settingsGroup.add(beginner);
        settingsGroup.add(intermediate);
        settingsGroup.add(expert);
        settingsGroup.add(special);

        String[] labels = {ROWS_STRING, COLUMNS_STRING, MINES_STRING};
        int numPairs = labels.length;
        JPanel panel = new JPanel(new SpringLayout());

        for (String label : labels) {
            JLabel currentLabel = new JLabel(label, JLabel.TRAILING);
            panel.add(currentLabel);
            JTextField textField = new JTextField(TEXT_FIELD_SIZE);
            currentLabel.setLabelFor(textField);
            panel.add(textField);

            switch (label) {
                case ROWS_STRING:
                    textField.setText(Integer.toString(GameLevelUtils.getLevelParametersByLevel(gameLevel).getRowNumber()));
                    this.height = textField;
                    break;
                case COLUMNS_STRING:
                    textField.setText(Integer.toString(GameLevelUtils.getLevelParametersByLevel(gameLevel).getColumnNumber()));
                    this.width = textField;
                    break;
                case MINES_STRING:
                    textField.setText(Integer.toString(GameLevelUtils.getLevelParametersByLevel(gameLevel).getMinesNumber()));
                    this.mines = textField;
            }
        }

        makeCompactGrid(panel,
                numPairs, GRID_COLUMNS,
                SPRING_PREFERRED_INITIAL_X, SPRING_PREFERRED_INITIAL_Y,
                SPRING_PREFERRED_X, SPRING_PREFERRED_Y
        );

        settingsPanel.setLayout(new GridLayout(GRID_LAYOUT_ROWS, GRID_LAYOUT_COLUMNS));

        settingsPanel.add(beginner);
        settingsPanel.add(intermediate);
        settingsPanel.add(expert);
        settingsPanel.add(special);

        add(settingsPanel, BorderLayout.NORTH);
        add(panel, BorderLayout.CENTER);

        JPanel okPanel = new JPanel();
        JButton okButton = new JButton("OK");
        okPanel.add(okButton);
        add(okPanel, BorderLayout.SOUTH);

        settingsPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Уровень"));

        panel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Параметры"));

        switch (gameLevel) {
            case BEGINNER:
                choosePoint(beginner, panel);
                break;
            case INTERMEDIATE:
                choosePoint(intermediate, panel);
                break;
            case EXPERT:
                choosePoint(expert, panel);
                break;
            case SPECIAL:
                special.setSelected(true);
        }

        beginner.addActionListener(e -> changeGameLevel(GameLevel.BEGINNER, panel));
        intermediate.addActionListener(e -> changeGameLevel(GameLevel.INTERMEDIATE, panel));
        expert.addActionListener(e -> changeGameLevel(GameLevel.EXPERT, panel));
        special.addActionListener(e -> {
            Component[] components = panel.getComponents();
            for (Component component : components) {
                component.setEnabled(true);
            }
            this.gameLevel = GameLevel.SPECIAL;
        });

        okButton.addActionListener(e -> {
            int rowNumber = Integer.parseInt(this.height.getText());
            int columnNumber = Integer.parseInt(this.width.getText());
            int minesNumber = Integer.parseInt(this.mines.getText());
            controller.changeLevelHandle(this.gameLevel);
            controller.changeLevelParameterHandle(Optional.ofNullable(
                    GameLevelUtils
                            .getLevelParametersByLevel(this.gameLevel)
            ).orElse(
                    new LevelParameter(rowNumber, columnNumber, minesNumber)));
            controller.restartNewGameHandle();
            dispose();
        });

        pack();
        setLocationRelativeTo(owner);
        setModal(true);
        setResizable(false);
        setVisible(true);
    }

    private static SpringLayout.Constraints getConstraintsForCell(
            int row, int col,
            Container parent,
            int cols) {
        SpringLayout layout = (SpringLayout) parent.getLayout();
        Component component = parent.getComponent(row * cols + col);
        return layout.getConstraints(component);
    }

    private static void makeCompactGrid(Container parent,
                                        int rows, int columns,
                                        int initialX, int initialY,
                                        int xPad, int yPad) {
        SpringLayout layout;

        if (!(parent.getLayout() instanceof SpringLayout)) {
            log.error("Первый аргумент метода makeCompactGrid должен использовать SpringLayout.");
            return;
        }
        layout = (SpringLayout) parent.getLayout();

        Spring horizontalSpring = Spring.constant(initialX);
        for (int column = 0; column < columns; column++) {
            Spring width = Spring.constant(0);

            for (int row = 0; row < rows; row++) {
                width = Spring.max(width, getConstraintsForCell(row, column, parent, columns).getWidth());
            }

            for (int r = 0; r < rows; r++) {
                SpringLayout.Constraints constraints = getConstraintsForCell(r, column, parent, columns);
                constraints.setX(horizontalSpring);
                constraints.setWidth(width);
            }
            horizontalSpring = Spring.sum(horizontalSpring, Spring.sum(width, Spring.constant(xPad)));
        }

        Spring verticalSpring = Spring.constant(initialY);
        for (int row = 0; row < rows; row++) {
            Spring height = Spring.constant(0);

            for (int column = 0; column < columns; column++) {
                height = Spring.max(height,
                        getConstraintsForCell(row, column, parent, columns).
                                getHeight());
            }

            for (int column = 0; column < columns; column++) {
                SpringLayout.Constraints constraints = getConstraintsForCell(row, column, parent, columns);
                constraints.setY(verticalSpring);
                constraints.setHeight(height);
            }
            verticalSpring = Spring.sum(verticalSpring, Spring.sum(height, Spring.constant(yPad)));
        }

        SpringLayout.Constraints parentConstraints = layout.getConstraints(parent);
        parentConstraints.setConstraint(SpringLayout.SOUTH, verticalSpring);
        parentConstraints.setConstraint(SpringLayout.EAST, horizontalSpring);
    }

    private void choosePoint(JRadioButton radioButton, JPanel panel) {
        Component[] components = panel.getComponents();
        for (Component component : components) {
            component.setEnabled(false);
        }
        radioButton.setSelected(true);
    }

    private void changeGameLevel(GameLevel gameLevel, JPanel panel) {
        Component[] components = panel.getComponents();

        for (Component component : components) {
            component.setEnabled(false);
        }

        setTextFieldValues(GameLevelUtils.getLevelParametersByLevel(gameLevel).getRowNumber(),
                GameLevelUtils.getLevelParametersByLevel(gameLevel).getColumnNumber(),
                GameLevelUtils.getLevelParametersByLevel(gameLevel).getMinesNumber());
        this.gameLevel = gameLevel;
    }

    private void setTextFieldValues(int height, int width, int amountOfMines) {
        this.height.setText(Integer.toString(height));
        this.width.setText(Integer.toString(width));
        this.mines.setText(Integer.toString(amountOfMines));
    }
}
