package ru.nsu.ccfit.legostaeva.validator;

public class PropertiesValueValidator implements Validator<Integer> {
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isValid(Integer value) {
        return value > 0;
    }
}
