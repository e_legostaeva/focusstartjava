package ru.nsu.ccfit.legostaeva.task;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode
@RequiredArgsConstructor
public class TaskProducer {
    private static final int TASK_WEIGHT = 100000;
    private final int upperLimit;

    /**
     * Создает объекты типа {@link Task} с диапазоном значений, зависящим от {@value #TASK_WEIGHT}.
     *
     * @return список созданных объектов.
     */
    public List<Task> produce() {
        int tasksWithFullSize = upperLimit / TASK_WEIGHT;
        int restNumbers = upperLimit % TASK_WEIGHT;
        int localStart = 1;
        int localEnd = 1;

        List<Task> tasks = new ArrayList<>();

        for (int i = 0; i < tasksWithFullSize; i++) {
            localEnd += TASK_WEIGHT;
            tasks.add(new Task(localStart, localEnd));
            localStart = localEnd;
        }

        if (restNumbers != 0) {
            localEnd += restNumbers;
            tasks.add(new Task(localStart, localEnd));
        }
        return tasks;
    }
}
