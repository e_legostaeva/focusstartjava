package ru.nsu.ccfit.legostaeva.validator;

public interface Validator<T> {
    /**
     * Проверяет допустимость значения
     *
     * @param value значение, которое нужно проверить
     * @return true, если значение допустимо, иначе false
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    boolean isValid(T value);
}
