package ru.nsu.ccfit.legostaeva;

import ru.nsu.ccfit.legostaeva.evaluator.Evaluator;
import ru.nsu.ccfit.legostaeva.evaluator.MultiplicationEvaluator;
import ru.nsu.ccfit.legostaeva.extractor.TableSizeExtractor;
import ru.nsu.ccfit.legostaeva.extractor.TableSizeExtractorImpl;
import ru.nsu.ccfit.legostaeva.printer.Printer;
import ru.nsu.ccfit.legostaeva.printer.StringPrinter;

import java.util.Optional;

public class Task1 {
    public static void main(String[] args) {
        TableSizeExtractor tableSizeExtractor = new TableSizeExtractorImpl();

        int tableSize;

        try {
            tableSize = Optional.ofNullable(tableSizeExtractor.getTableSizeFromArguments(args))
                    .orElseGet(() -> tableSizeExtractor.getTableSizeFromInputStream(System.in));
        } catch (IllegalStateException e) {
            System.out.println("Error while reading from input");
            return;
        }

        Evaluator multiplicationEvaluator = new MultiplicationEvaluator();
        int[][] multiplicationTable = multiplicationEvaluator.evaluate(tableSize);

        Printer stringFormatter = new StringPrinter();
        stringFormatter.printFormatted(multiplicationTable);
    }
}