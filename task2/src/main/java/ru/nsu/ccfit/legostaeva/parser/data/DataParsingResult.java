package ru.nsu.ccfit.legostaeva.parser.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.nsu.ccfit.legostaeva.figure.FigureType;

import javax.annotation.ParametersAreNonnullByDefault;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@ToString
@ParametersAreNonnullByDefault
public class DataParsingResult {
    private final List<BigDecimal> params = new ArrayList<>();
    private FigureType name;

    void addParameter(BigDecimal parameter) {
        params.add(parameter);
    }
}
