package ru.nsu.ccfit.legostaeva.view;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.swing.*;

/**
 * Игровая ячейка.
 */
@Getter
@RequiredArgsConstructor
class ViewCell extends JLabel {
    private final int row;
    private final int column;
}
