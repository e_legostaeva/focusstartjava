package ru.nsu.ccfit.legostaeva.figure;

import com.google.common.collect.ImmutableList;
import lombok.ToString;
import ru.nsu.ccfit.legostaeva.exception.NotCorrectDataException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.math.BigDecimal;
import java.util.List;

import static ru.nsu.ccfit.legostaeva.figure.Parameter.*;

@ToString
@ParametersAreNonnullByDefault
public abstract class Figure {
    /**
     * Получает параметры данной фигуры.
     */
    @Nonnull
    public abstract List<ParameterValue> getParameters();

    /**
     * Получает тип данной фигуры.
     */
    @Nonnull
    protected abstract FigureType getFigureType();

    /**
     * Считает площадь данной фигуры.
     */
    @Nonnull
    protected abstract BigDecimal getArea();

    /**
     * Считает периметр данной фигуры.
     */
    @Nonnull
    protected abstract BigDecimal getPerimeter();

    /**
     * Сравнивает количество параметров, необходимых для инициализации данной фигуры, с числом полученных параметров.
     *
     * @param parameters входные параметры фигуры.
     * @throws NotCorrectDataException если число входных параметров фигуры не совпадает с необходимым.
     */
    protected void validateParametersNumber(List<BigDecimal> parameters) throws NotCorrectDataException {
        if (parameters.size() != getParametersNumber()) {
            throw new NotCorrectDataException(
                    "Для инициализации " + getFigureType() +
                            " необходимо " + getParametersNumber() +
                            " параметров, передано: " + parameters.size()
            );
        }
    }

    /**
     * Определяет количество параметров, необходимых для инициализации данной фигуры.
     */
    @Nonnull
    protected abstract Long getParametersNumber();

    /**
     * Определяет свойства, принадлежащие каждой фигуре: название, площадь, периметр.
     */
    protected ImmutableList.Builder<ParameterValue> getDefaultProperties() {
        return new ImmutableList.Builder<ParameterValue>()
                .add(new ParameterValue(NAME, getFigureType().getReadableName()))
                .add(new ParameterValue(AREA, getArea().toString()))
                .add(new ParameterValue(PERIMETER, getPerimeter().toString()));
    }

}