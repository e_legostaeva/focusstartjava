package ru.nsu.ccfit.legostaeva.figure.implementations;

import lombok.ToString;
import ru.nsu.ccfit.legostaeva.figure.Figure;
import ru.nsu.ccfit.legostaeva.figure.FigureType;
import ru.nsu.ccfit.legostaeva.figure.ParameterValue;
import ru.nsu.ccfit.legostaeva.exception.NotCorrectDataException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static ru.nsu.ccfit.legostaeva.figure.Parameter.OPPOSITE_ANGLE;
import static ru.nsu.ccfit.legostaeva.figure.Parameter.SIDE_LENGTH;

@ToString
@ParametersAreNonnullByDefault
public class Triangle extends Figure {
    private final BigDecimal firstSide;
    private final BigDecimal secondSide;
    private final BigDecimal thirdSide;

    public Triangle(List<BigDecimal> parameters) throws NotCorrectDataException {
        validateParametersNumber(parameters);
        firstSide = parameters.get(0).setScale(2, RoundingMode.HALF_UP);
        secondSide = parameters.get(1).setScale(2, RoundingMode.HALF_UP);
        thirdSide = parameters.get(2).setScale(2, RoundingMode.HALF_UP);
        if (!triangleExist()) {
            throw new NotCorrectDataException("Треугольник с параметрами " + parameters + " не существует");
        }
    }

    @Nonnull
    @Override
    public List<ParameterValue> getParameters() {
        return getDefaultProperties()
                .add(new ParameterValue(SIDE_LENGTH, firstSide.toString()))
                .add(new ParameterValue(OPPOSITE_ANGLE, getOppositeAngle(firstSide, secondSide, thirdSide).toString()))
                .add(new ParameterValue(SIDE_LENGTH, secondSide.toString()))
                .add(new ParameterValue(OPPOSITE_ANGLE, getOppositeAngle(secondSide, firstSide, thirdSide).toString()))
                .add(new ParameterValue(SIDE_LENGTH, thirdSide.toString()))
                .add(new ParameterValue(OPPOSITE_ANGLE, getOppositeAngle(thirdSide, secondSide, firstSide).toString()))
                .build();
    }

    @Nonnull
    @Override
    protected FigureType getFigureType() {
        return FigureType.TRIANGLE;
    }

    @Nonnull
    @Override
    protected BigDecimal getArea() {
        BigDecimal halfPerimeter = getPerimeter().divide(BigDecimal.valueOf(2), RoundingMode.HALF_UP);
        return BigDecimal
                .valueOf(
                        Math.sqrt(
                                halfPerimeter
                                        .multiply(halfPerimeter.subtract(firstSide))
                                        .multiply(halfPerimeter.subtract(secondSide))
                                        .multiply(halfPerimeter.subtract(thirdSide))
                                        .doubleValue()
                        )
                )
                .setScale(2, RoundingMode.HALF_UP);
    }


    @Nonnull
    private BigDecimal getOppositeAngle(BigDecimal oppositeSide, BigDecimal rightSide, BigDecimal leftSide) {
        return BigDecimal
                .valueOf(
                        Math.toDegrees(
                                Math.acos(((rightSide
                                        .multiply(rightSide)
                                        .add(leftSide.multiply(leftSide))
                                        .subtract(oppositeSide.multiply(oppositeSide)))
                                        .divide(BigDecimal.valueOf(2)
                                                        .multiply(rightSide)
                                                        .multiply(leftSide),
                                                RoundingMode.HALF_UP)
                                ).doubleValue()))
                )
                .setScale(2, RoundingMode.HALF_UP);
    }

    @Nonnull
    @Override
    protected BigDecimal getPerimeter() {
        return firstSide.add(secondSide).add(thirdSide).setScale(2, RoundingMode.HALF_UP);
    }

    @Nonnull
    @Override
    protected Long getParametersNumber() {
        return 3L;
    }

    private boolean triangleExist() {
        return (firstSide.add(secondSide).compareTo(thirdSide) > 0)
                && (secondSide.add(thirdSide).compareTo(firstSide) > 0)
                && (firstSide.add(thirdSide).compareTo(secondSide) > 0);
    }
}
