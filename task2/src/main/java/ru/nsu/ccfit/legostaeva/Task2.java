package ru.nsu.ccfit.legostaeva;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.ParseException;
import ru.nsu.ccfit.legostaeva.figure.Figure;
import ru.nsu.ccfit.legostaeva.figure.FigureFactory;
import ru.nsu.ccfit.legostaeva.file.FileProvider;
import ru.nsu.ccfit.legostaeva.parser.argument.ArgumentParser;
import ru.nsu.ccfit.legostaeva.parser.argument.ArgumentsParsingResult;
import ru.nsu.ccfit.legostaeva.parser.data.DataParser;
import ru.nsu.ccfit.legostaeva.parser.data.DataParsingResult;
import ru.nsu.ccfit.legostaeva.printer.Printer;
import ru.nsu.ccfit.legostaeva.printer.PrinterImpl;
import ru.nsu.ccfit.legostaeva.exception.NotCorrectDataException;

import java.io.*;
import java.util.Optional;

@Slf4j
public class Task2 {
    public static void main(String[] args) {
        File inputFile;
        FileProvider fileProvider = new FileProvider();
        ArgumentParser argumentParser = new ArgumentParser();
        ArgumentsParsingResult argumentsParsingResult;
        try {
            argumentsParsingResult = argumentParser.parseArguments(args);
        } catch (ParseException e) {
            log.error("Возникли проблемы при обработке командной строки.", e);
            return;
        }

        try {
            inputFile = fileProvider.getInputFile(argumentsParsingResult.getInputFileName());
        } catch (NotCorrectDataException e) {
            log.error("Входной файл и файл по умолчанию не существуют, либо нет прав на чтение.", e);
            return;
        }

        DataParsingResult dataParsingResult;
        try (InputStream inputStream = new FileInputStream(inputFile)) {
            DataParser parser = new DataParser();
            dataParsingResult = parser.parseArguments(inputStream);
        } catch (NotCorrectDataException | IOException | IllegalArgumentException e) {
            log.error("Ошибка при обработке входного файла.", e);
            return;
        }

        Figure figure;
        try {
            FigureFactory figureFactory = new FigureFactory();
            figure = figureFactory.getFigure(dataParsingResult.getName(), dataParsingResult.getParams());
        } catch (NotCorrectDataException e) {
            log.error("Параметры не подходят для типа фигуры.", e);
            return;
        }

        File outputFile = null;
        try {
            outputFile = fileProvider.getOutputFile(argumentsParsingResult.getOutputFileName());
        } catch (NotCorrectDataException e) {
            log.error("Ошибка при открытии файла вывода", e);
        }

        try (OutputStream closingOutputStream = outputFile == null ?
                null
                : new FileOutputStream(outputFile)
        ) {
            Printer printer = new PrinterImpl();
            printer.printParameters(figure.getParameters(), Optional.ofNullable(closingOutputStream).orElse(System.out));
        } catch (FileNotFoundException e) {
            log.error("Выходной файл не существует или не может быть открыт по другой причине.", e);
        } catch (IOException e) {
            log.error("Ошибка вывода.", e);
        }
    }
}
