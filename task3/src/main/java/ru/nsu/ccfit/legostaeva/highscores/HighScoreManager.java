package ru.nsu.ccfit.legostaeva.highscores;

import com.google.common.collect.ImmutableMap;
import lombok.extern.slf4j.Slf4j;
import ru.nsu.ccfit.legostaeva.exception.MinesweeperException;
import ru.nsu.ccfit.legostaeva.level.GameLevel;

import java.io.*;
import java.net.URL;
import java.util.*;

/**
 * Класс, обрабатывающий рекорды. Отвечает за хранение, обновление и получение файлов рекордов.
 */
@Slf4j
public class HighScoreManager {
    private static final String BEGINNER_HIGH_SCORE_FILE = "scores/beginner_scores.dat";
    private static final String INTERMEDIATE_HIGH_SCORE_FILE = "scores/intermediate_scores.dat";
    private static final String EXPERT_HIGH_SCORE_FILE = "scores/expert_scores.dat";
    private static final Map<GameLevel, String> LEVEL_FILE_NAMES = ImmutableMap.of(
            GameLevel.BEGINNER, BEGINNER_HIGH_SCORE_FILE,
            GameLevel.INTERMEDIATE, INTERMEDIATE_HIGH_SCORE_FILE,
            GameLevel.EXPERT, EXPERT_HIGH_SCORE_FILE
    );
    private static final int MAX_SCORES_SIZE = 10;
    private static final String NAME_SEPARATOR = ". ";
    private static final String SCORE_SEPARATOR = ":  ";

    private Map<GameLevel, List<Score>> scores;
    private ObjectOutputStream outputStream = null;
    private ObjectInputStream inputStream = null;

    public HighScoreManager() {
        scores = new HashMap<>();
        for (GameLevel gameLevel : GameLevel.values()) {
            if (gameLevel != GameLevel.SPECIAL) scores.put(gameLevel, new ArrayList<>());
        }
    }

    private List<Score> getScores(GameLevel gameLevel) throws MinesweeperException {
        loadScoreFile(gameLevel);
        sort(gameLevel);
        updateScoreFile(gameLevel);
        return scores.get(gameLevel);
    }

    private void sort(GameLevel gameLevel) {
        ScoreComparator comparator = new ScoreComparator();
        scores.get(gameLevel).sort(comparator);
    }

    public void addScore(String name, int score, GameLevel gameLevel) throws MinesweeperException {
        scores.get(gameLevel).add(new Score(name, score));
        updateScoreFile(gameLevel);
    }

    @SuppressWarnings("unchecked")
    //так как уверена, что inputStream.readObject() считает тип, приводимый к List<Score>
    private void loadScoreFile(GameLevel gameLevel) throws MinesweeperException {
        if (gameLevel != GameLevel.SPECIAL) {
            URL fileUrl = getClass().getClassLoader().getResource(LEVEL_FILE_NAMES.get(gameLevel));
            try {
                File levelFileName = Optional.ofNullable(fileUrl)
                        .map(URL::getFile)
                        .map(File::new)
                        .orElseThrow(() -> new MinesweeperException("Не найден файл " + LEVEL_FILE_NAMES.get(gameLevel)));
                inputStream = new ObjectInputStream(new FileInputStream(levelFileName));
                scores.put(gameLevel, (List<Score>) inputStream.readObject());
            } catch (IOException e) {
                log.error("Произошла ошибка во время ввода/вывода.", e);
            } catch (ClassNotFoundException e) {
                throw new MinesweeperException("Ошибка во время чтения файла рекордов.");
            }

            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                log.error("Произошла ошибка во время ввода/вывода.", e);
            }
        }
    }

    private void updateScoreFile(GameLevel gameLevel) throws MinesweeperException {
        if (gameLevel != GameLevel.SPECIAL) {
            URL fileUrl = getClass().getClassLoader().getResource(LEVEL_FILE_NAMES.get(gameLevel));
            try {
                File defaultInputFile = Optional.ofNullable(fileUrl)
                        .map(URL::getFile)
                        .map(File::new)
                        .orElseThrow(() -> new MinesweeperException("Не найден файл " + LEVEL_FILE_NAMES.get(gameLevel)));
                outputStream = new ObjectOutputStream(new FileOutputStream(defaultInputFile));
                outputStream.writeObject(scores.get(gameLevel));
            } catch (IOException e) {
                log.warn("Произошла ошибка во время ввода/вывода.", e);
            } finally {
                try {
                    if (outputStream != null) {
                        outputStream.flush();
                        outputStream.close();
                    }
                } catch (IOException e) {
                    log.error("Произошла ошибка во время ввода/вывода.", e);
                }
            }
        }
    }

    public String getHighScoreString(GameLevel gameLevel) throws MinesweeperException {
        StringBuilder highScoreString = new StringBuilder();
        List<Score> scores = getScores(gameLevel);
        int currentScore = 0;
        int scoresSize = scores.size();

        if (scoresSize > MAX_SCORES_SIZE) {
            scoresSize = MAX_SCORES_SIZE;
        }

        while (currentScore < scoresSize) {
            highScoreString.append(currentScore + 1)
                    .append(NAME_SEPARATOR)
                    .append(scores.get(currentScore).getName())
                    .append(SCORE_SEPARATOR)
                    .append(scores.get(currentScore).getScore())
                    .append(System.lineSeparator());
            currentScore++;
        }

        return highScoreString.toString();
    }
}
