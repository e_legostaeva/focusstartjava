package ru.nsu.ccfit.legostaeva.figure.supplier;

import ru.nsu.ccfit.legostaeva.figure.implementations.Triangle;
import ru.nsu.ccfit.legostaeva.exception.NotCorrectDataException;

import java.math.BigDecimal;
import java.util.List;

/**
 * {@inheritDoc}
 */
public class TriangleSupplier implements FigureSupplier<Triangle> {
    @Override
    public Triangle getFigure(List<BigDecimal> figureParameters) throws NotCorrectDataException {
        return new Triangle(figureParameters);
    }
}
