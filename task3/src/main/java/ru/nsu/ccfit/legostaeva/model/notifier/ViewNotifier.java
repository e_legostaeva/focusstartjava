package ru.nsu.ccfit.legostaeva.model.notifier;

import ru.nsu.ccfit.legostaeva.level.GameLevel;
import ru.nsu.ccfit.legostaeva.view.MinesweeperView;

/**
 * Уведомитель. Используется для того, чтобы оповещать {@link MinesweeperView} об изменениях,
 * происходящих в {@link ru.nsu.ccfit.legostaeva.model.MinesweeperModel}.
 */
public interface ViewNotifier {
    void attachView(MinesweeperView minesweeperView);

    void notifyViewsNewGame(int rowNumber, int columnNumber, int minesNumber);

    void notifyViewsNewCellChanged(int row, int column, String cellType);

    void notifyViewsAboutGameOver(String[][] fieldTypes);

    void notifyViewsAboutGameWon(GameLevel gameLevel);

    void notifyViewAboutFlaggedCellNumber(int flaggedCellNumber, int minesNumber);

    void notifyViewAboutTimeChange(int timeSeconds);

    void notifyViewAboutFailWhileAddingNewScore();

    void notifyViewAboutFailWhileLoadingScores();

    void notifyViewAboutShowScores(String scores);

    void notifyViewAboutTooBigGameParameters(int maxRows, int maxColumns, int maxMinesAmount);

    void notifyViewAboutLittleGameParameters(int minRows, int minColumns, int minMinesAmount);

    void notifyViewAboutTooBigMinesAmount(int maxMinesAmount);
}
