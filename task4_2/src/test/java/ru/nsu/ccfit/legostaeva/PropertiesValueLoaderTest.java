package ru.nsu.ccfit.legostaeva;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.nsu.ccfit.legostaeva.exception.NotCorrectDataException;
import ru.nsu.ccfit.legostaeva.valueloader.PropertiesValueLoader;
import ru.nsu.ccfit.legostaeva.valueloader.PropertiesValueLoaderResult;

import java.io.IOException;

@DisplayName("Тесты на загрузчик значений .properties")
class PropertiesValueLoaderTest {
    private final PropertiesValueLoader propertiesValueLoader = new PropertiesValueLoader();

    @Test
    @DisplayName("Проверка корректных значений .properties")
    void load_returnPropertiesValues_ifPropertiesCorrect() throws IOException, NotCorrectDataException {
        PropertiesValueLoaderResult actualValueLoaderResult = propertiesValueLoader.loadPropertiesValues(
                ClassLoader.getSystemResourceAsStream("good.properties")
        );

        PropertiesValueLoaderResult expectedValueLoaderResult = new PropertiesValueLoaderResult()
                .setProducerThreadsNumber(1)
                .setConsumerThreadsNumber(2)
                .setTimeForProducerThreads(3)
                .setTimeForConsumerThreads(4)
                .setStorageSize(5);

        Assertions.assertThat(expectedValueLoaderResult).isEqualToComparingFieldByFieldRecursively(actualValueLoaderResult);
    }

    @Test
    @DisplayName("Проверка корректной обработки значений .properties, содержащих пустые значения")
    void load_throwNotCorrectDataException_ifPropertiesContainsNull() {
        Assertions.assertThatThrownBy(() -> propertiesValueLoader.loadPropertiesValues(
                ClassLoader.getSystemResourceAsStream("nullValue.properties")
                )
        )
                .isInstanceOf(NotCorrectDataException.class)
                .hasMessage("Отсутствует числовое значение в .properties");
    }

    @Test
    @DisplayName("Проверка корректной обработки значений .properties, содержащих строки")
    void load_throwNotCorrectDataException_ifPropertiesContainsString() {
        Assertions.assertThatThrownBy(() -> propertiesValueLoader.loadPropertiesValues(
                ClassLoader.getSystemResourceAsStream("stringValue.properties")
                )
        )
                .isInstanceOf(NotCorrectDataException.class)
                .hasMessage("Отсутствует числовое значение в .properties");
    }
}