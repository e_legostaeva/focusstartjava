package ru.nsu.ccfit.legostaeva.exception;

/**
 * Исключение, связанное с некорректной работой программы.
 */
public class MinesweeperException extends Exception {
    public MinesweeperException(String message) {
        super(message);
    }
}
