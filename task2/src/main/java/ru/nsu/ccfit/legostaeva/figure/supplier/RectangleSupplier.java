package ru.nsu.ccfit.legostaeva.figure.supplier;

import ru.nsu.ccfit.legostaeva.figure.implementations.Rectangle;
import ru.nsu.ccfit.legostaeva.exception.NotCorrectDataException;

import java.math.BigDecimal;
import java.util.List;

/**
 * {@inheritDoc}
 */
public class RectangleSupplier implements FigureSupplier<Rectangle> {
    @Override
    public Rectangle getFigure(List<BigDecimal> figureParameters) throws NotCorrectDataException {
        return new Rectangle(figureParameters);
    }
}
