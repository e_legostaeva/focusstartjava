package ru.nsu.ccfit.legostaeva.validator;

import java.io.File;

public class FileValidator implements Validator<File> {
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isValid(File file) {
        return (file.exists()) && (file.canRead());
    }
}
