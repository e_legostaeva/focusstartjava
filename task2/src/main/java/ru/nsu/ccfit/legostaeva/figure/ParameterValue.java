package ru.nsu.ccfit.legostaeva.figure;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public final class ParameterValue {
    private final Parameter parameter;

    private final String value;
}
