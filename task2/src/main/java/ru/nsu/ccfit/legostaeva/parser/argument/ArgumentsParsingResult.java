package ru.nsu.ccfit.legostaeva.parser.argument;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ArgumentsParsingResult {
    private String inputFileName;

    private String outputFileName;
}
