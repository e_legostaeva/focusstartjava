package ru.nsu.ccfit.legostaeva;

import com.google.common.collect.ImmutableList;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.nsu.ccfit.legostaeva.task.Task;
import ru.nsu.ccfit.legostaeva.task.TaskProducer;

import java.util.List;

@DisplayName("Тесты на обработчик заданий")
class TaskProducerTest {
    private List<Task> tasks = ImmutableList.of(
            new Task(1, 101),
            new Task(101, 201),
            new Task(201, 301),
            new Task(301, 401),
            new Task(401, 501),
            new Task(501, 601),
            new Task(601, 631)
    );

    @Test
    @DisplayName("Проверка создания списка задач корректного веса")
    void produce_returnListOfTasks_ifLimitIs630() {
        TaskProducer taskProducer = new TaskProducer(630);
        Assertions.assertThat(tasks)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyElementsOf(taskProducer.produce());
    }
}