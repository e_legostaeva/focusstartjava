package ru.nsu.ccfit.legostaeva.printer;

import org.apache.commons.lang3.StringUtils;

public class StringPrinter implements Printer {
    private static final String HORIZONTAL_CELL_SIDE = "-";
    private static final String VERTICAL_CELL_SIDE = "|";
    private static final String CELL_ANGLE = "+";

    /**
     * {@inheritDoc}
     */
    @Override
    public void printFormatted(int[][] table) {
        int cellSize = getMaximumCellSize(table);

        String format = "%" + cellSize + "d" + VERTICAL_CELL_SIDE;
        String lastSymbolFormat = "%" + cellSize + "d";
        String endCellBound = StringUtils.repeat(HORIZONTAL_CELL_SIDE, cellSize);
        String cellBound = endCellBound.concat(CELL_ANGLE);

        drawFirstLine(table.length, format, cellSize, cellBound, endCellBound);

        for (int i = 0; i < table.length; ++i) {
            System.out.println();
            System.out.printf(format, i + 1);
            for (int j = 0; j < table[i].length; ++j) {
                if (j == (table[i].length - 1)) {
                    System.out.printf(lastSymbolFormat, table[i][j]);
                } else {
                    System.out.printf(format, table[i][j]);
                }
            }
            drawSeparationLine(cellBound, endCellBound, table.length);
        }
    }

    private void drawFirstLine(int tableSize, String format, int cellSize, String cellBound, String endCellBound) {
        for (int l = 0; l < cellSize; l++) {
            System.out.print(" ");
        }
        System.out.print(VERTICAL_CELL_SIDE);
        for (int j = 1; j <= tableSize; j++) {
            if (j == tableSize) {
                format = "%" + cellSize + "d";
            }
            System.out.printf(format, j);
        }
        drawSeparationLine(cellBound, endCellBound, tableSize);
    }

    private void drawSeparationLine(String cellBound, String endCellBound, int length) {
        System.out.println();
        System.out.print(StringUtils.repeat(cellBound, length));
        System.out.print(endCellBound);
    }

    private int getMaximumCellSize(int[][] table) {
        int cellSize = 0;
        for (int i = 0; i < table.length; ++i) {
            for (int j = 0; j < table[i].length; ++j) {
                int curCountOfDigitsInNumber = String.valueOf(table[i][j]).length();
                if (cellSize < curCountOfDigitsInNumber) {
                    cellSize = curCountOfDigitsInNumber;
                }
            }
        }
        return cellSize;
    }
}