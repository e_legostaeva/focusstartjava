package ru.nsu.ccfit.legostaeva.view;

import ru.nsu.ccfit.legostaeva.level.GameLevel;

/**
 * Представление - отвечает за отображение данных из {@link ru.nsu.ccfit.legostaeva.model.MinesweeperModel}
 * пользователю, реагируя на изменения, приходящие оттуда.
 */
public interface MinesweeperView {
    void renderNewGame(int rowNumber, int columnNumber, int minesNumber);

    void updateCell(int row, int column, String cellType);

    void renderGameLost(String[][] fieldTypes);

    void renderGameWon(GameLevel gameLevel);

    void updateMinesCounter(int flaggedCellNumber, int minesNumber);

    void updateTimer(int timeSeconds);

    void renderFailWhileAddingNewScore();

    void renderFailWhileLoadingScores();

    void showHighScores(String currentScores);

    void renderTooBigGameParameters(int maxRows, int maxColumns, int maxMinesAmount);

    void renderLittleGameParameters(int minRows, int minColumns, int minMinesAmount);

    void renderTooBigMinesAmount(int maxMinesAmount);
}
