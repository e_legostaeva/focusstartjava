package ru.nsu.ccfit.legostaeva.figure.supplier;

import ru.nsu.ccfit.legostaeva.figure.Figure;
import ru.nsu.ccfit.legostaeva.exception.NotCorrectDataException;

import java.math.BigDecimal;
import java.util.List;

/**
 * Возвращает фигуру типа T.
 *
 * @param <T> - тип фигуры.
 */
public interface FigureSupplier<T extends Figure> {
    T getFigure(List<BigDecimal> figureParameters) throws NotCorrectDataException;
}
