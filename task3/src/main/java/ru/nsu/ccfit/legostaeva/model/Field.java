package ru.nsu.ccfit.legostaeva.model;

import lombok.Getter;

import java.util.Random;

/**
 * Игровое поле.
 */
@Getter
class Field {
    private final Cell[][] cells;

    private final int rowNumber;
    private final int columnNumber;
    private final int minesNumber;

    Field(int rowNumber, int columnNumber, int minesNumber) {
        this.rowNumber = rowNumber;
        this.columnNumber = columnNumber;
        this.minesNumber = minesNumber;
        this.cells = new Cell[rowNumber][columnNumber];
        initializeCells(rowNumber, columnNumber);
    }

    private void initializeCells(int rowNumber, int columnNumber) {
        for (int i = 0; i < rowNumber; i++) {
            for (int j = 0; j < columnNumber; j++) {
                cells[i][j] = new Cell(i, j);
            }
        }
    }

    void initializeField(Cell firstOpenCell) {
        placeAllMines(firstOpenCell);
        calculateField();
    }

    private void placeAllMines(Cell firstOpenedCell) {
        int[] tempArray = new int[rowNumber * columnNumber];

        for (int i = 0; i < tempArray.length; ++i) {
            tempArray[i] = i;
        }

        Random random = new Random();
        int numberOfFirstOpenedCell = columnNumber * firstOpenedCell.getRow() + firstOpenedCell.getColumn();

        tempArray[0] = numberOfFirstOpenedCell;
        tempArray[numberOfFirstOpenedCell] = 0;

        for (int i = 1; i < minesNumber + 1; ++i) {
            int tmpMine = random.nextInt(tempArray.length - i) + i;
            int tmp = tempArray[i];
            tempArray[i] = tempArray[tmpMine];
            tempArray[tmpMine] = tmp;
            cells[tempArray[i] / columnNumber][tempArray[i] % columnNumber].setMined(true);
        }
    }

    private void calculateField() {
        for (int i = 0; i < rowNumber; ++i) {
            for (int j = 0; j < columnNumber; ++j) {
                if (!cells[i][j].isMined()) continue;
                if ((i - 1 >= 0) && (j - 1 >= 0)) {
                    cells[i - 1][j - 1].increaseNumberOfMinesAround();
                }
                if ((i - 1 >= 0)) {
                    cells[i - 1][j].increaseNumberOfMinesAround();
                }
                if ((i - 1 >= 0) && (j + 1 < columnNumber)) {
                    cells[i - 1][j + 1].increaseNumberOfMinesAround();
                }
                if ((j - 1 >= 0)) {
                    cells[i][j - 1].increaseNumberOfMinesAround();
                }
                if ((j + 1 < columnNumber)) {
                    cells[i][j + 1].increaseNumberOfMinesAround();
                }
                if ((i + 1 < rowNumber) && (j - 1 >= 0)) {
                    cells[i + 1][j - 1].increaseNumberOfMinesAround();
                }
                if ((i + 1 < rowNumber)) {
                    cells[i + 1][j].increaseNumberOfMinesAround();
                }
                if ((i + 1 < rowNumber) && (j + 1 < columnNumber)) {
                    cells[i + 1][j + 1].increaseNumberOfMinesAround();
                }
            }
        }
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    boolean checkCellExist(int row, int column) {
        return row >= 0 && row < rowNumber && column >= 0 && column < columnNumber;
    }

    Cell fetchCellByCoords(int row, int column) {
        return cells[row][column];
    }

    boolean checkAllMinesAreFlagged() {
        for (int i = 0; i < columnNumber; i++) {
            for (int j = 0; j < rowNumber; j++) {
                if ((cells[i][j].isMined() && (cells[i][j].getCurrentCellState() != CellState.FLAGGED)) ||
                        (!cells[i][j].isMined() && (cells[i][j].getCurrentCellState() == CellState.FLAGGED))) {
                    return false;
                }
            }
        }
        return true;
    }

    boolean checkAllMineFreeCellsAreOpened() {
        for (int i = 0; i < columnNumber; i++) {
            for (int j = 0; j < rowNumber; j++) {
                if (!cells[i][j].isMined() && cells[i][j].getCurrentCellState() != CellState.OPENED) {
                    return false;
                }
            }
        }
        return true;
    }
}
