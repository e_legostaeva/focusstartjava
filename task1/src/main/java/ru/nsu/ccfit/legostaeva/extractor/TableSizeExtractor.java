package ru.nsu.ccfit.legostaeva.extractor;

import javax.annotation.Nullable;
import javax.annotation.WillNotClose;
import java.io.InputStream;

public interface TableSizeExtractor {
    /**
     * Получить размер таблицы из аргументов программы
     *
     * @param args аргументы программы
     * @return размер таблицы, либо null, если аргумент не задан, либо некорректен
     */
    @Nullable
    Integer getTableSizeFromArguments(String[] args);

    /**
     * Получить размер таблицы из входного потока данных
     *
     * @param inputStream входной поток данных
     * @return размер таблицы
     */
    int getTableSizeFromInputStream(@WillNotClose InputStream inputStream);
}