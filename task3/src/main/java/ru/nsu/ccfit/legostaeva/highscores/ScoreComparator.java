package ru.nsu.ccfit.legostaeva.highscores;

import java.util.Comparator;

/**
 * Класс, отвечающий за сравнение рекордов из {@link HighScoreManager}.
 */
public class ScoreComparator implements Comparator<Score> {
    /**
     * Сравнивает рекордные записи.
     *
     * @param score1 первая рекордная запись.
     * @param score2 вторая рекордная запись.
     * @return запись, рекордное значение которой больше.
     */
    @Override
    public int compare(Score score1, Score score2) {
        int sc1 = score1.getScore();
        int sc2 = score2.getScore();

        return Integer.compare(sc1, sc2);
    }
}
