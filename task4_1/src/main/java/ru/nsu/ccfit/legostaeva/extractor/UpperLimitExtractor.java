package ru.nsu.ccfit.legostaeva.extractor;

import javax.annotation.Nullable;
import javax.annotation.WillNotClose;
import java.io.InputStream;

public interface UpperLimitExtractor {
    /**
     * Получить верхнюю границу диапазона из аргументов программы
     *
     * @param args аргументы программы
     * @return верхня граница диапазона, либо null, если аргумент не задан, либо некорректен
     */
    @Nullable
    Integer getUpperLimitFromArguments(String[] args);

    /**
     * Получить верхнюю границу диапазона из входного потока данных
     *
     * @param inputStream входной поток данных
     * @return верхня граница диапазона
     */
    Integer getUpperLimitFromInputStream(@WillNotClose InputStream inputStream);
}
