package ru.nsu.ccfit.legostaeva.valueloader;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class PropertiesValueLoaderResult {
    private int producerThreadsNumber;
    private int consumerThreadsNumber;
    private int timeForProducerThreads;
    private int timeForConsumerThreads;
    private int storageSize;
}
