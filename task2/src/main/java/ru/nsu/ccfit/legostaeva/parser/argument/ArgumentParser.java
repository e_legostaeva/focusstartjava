package ru.nsu.ccfit.legostaeva.parser.argument;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.*;

import javax.annotation.Nonnull;

@Slf4j
public class ArgumentParser {
    private static final String INPUT_FILE_OPTION_NAME = "i";
    private static final String OUTPUT_FILE_OPTION_NAME = "o";
    private static final String INPUT_FILE_LONG_OPTION_NAME = "input";
    private static final String OUTPUT_FILE_LONG_OPTION_NAME = "output";
    private static final CommandLineParser COMMAND_LINE_PARSER = new DefaultParser();
    private static final Options ARGUMENTS_OPTIONS = new Options()
            .addOption(Option.builder(INPUT_FILE_OPTION_NAME)
                    .required(false)
                    .longOpt(INPUT_FILE_LONG_OPTION_NAME)
                    .hasArg(true)
                    .build()
            )
            .addOption(Option.builder(OUTPUT_FILE_OPTION_NAME)
                    .required(false)
                    .longOpt(OUTPUT_FILE_LONG_OPTION_NAME)
                    .hasArg(true)
                    .build());

    /**
     * Обрабатывает командную строку, извлекая имена входного и выходного файла.
     *
     * @param args аргументы программы.
     * @return {@link ArgumentsParsingResult}, который содержит имена.
     * входного и выходного файлов, если они существуют.
     * @throws ParseException если возникли проблемы при обработке командной строки.
     */
    @Nonnull
    public ArgumentsParsingResult parseArguments(String[] args) throws ParseException {
        ArgumentsParsingResult argumentsParsingResult = new ArgumentsParsingResult();
        CommandLine commandLine = COMMAND_LINE_PARSER.parse(ARGUMENTS_OPTIONS, args);

        argumentsParsingResult.setInputFileName(commandLine.getOptionValue(INPUT_FILE_OPTION_NAME));
        argumentsParsingResult.setOutputFileName(commandLine.getOptionValue(OUTPUT_FILE_OPTION_NAME));

        log.debug("Результат: {}", argumentsParsingResult);
        return argumentsParsingResult;
    }
}
