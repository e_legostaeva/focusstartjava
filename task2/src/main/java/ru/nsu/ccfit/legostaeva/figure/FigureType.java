package ru.nsu.ccfit.legostaeva.figure;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Типы используемых фигур.
 */
@Getter
@RequiredArgsConstructor
public enum FigureType {
    CIRCLE("Круг"),
    RECTANGLE("Прямоугольник"),
    TRIANGLE("Треугольник");

    private final String readableName;
}
