package ru.nsu.ccfit.legostaeva.evaluator;

public abstract class AbstractEvaluator implements Evaluator {
    /**
     * {@inheritDoc}
     */
    @Override
    public int[][] evaluate(int size) {
        int[][] valueTable = new int[size][size];

        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                valueTable[i][j] = performOperation(i + 1, j + 1);
            }
        }

        return valueTable;
    }

    /**
     * Выполняет заданную операцию
     *
     * @param a первый параметр операции
     * @param b второй параметр операции
     * @return результат выполненной операции
     */
    abstract int performOperation(int a, int b);
}
