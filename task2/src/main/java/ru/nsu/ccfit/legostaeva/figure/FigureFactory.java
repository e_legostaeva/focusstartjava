package ru.nsu.ccfit.legostaeva.figure;

import com.google.common.collect.ImmutableMap;
import lombok.extern.slf4j.Slf4j;
import ru.nsu.ccfit.legostaeva.figure.supplier.CircleSupplier;
import ru.nsu.ccfit.legostaeva.figure.supplier.FigureSupplier;
import ru.nsu.ccfit.legostaeva.figure.supplier.RectangleSupplier;
import ru.nsu.ccfit.legostaeva.figure.supplier.TriangleSupplier;
import ru.nsu.ccfit.legostaeva.exception.NotCorrectDataException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Slf4j
@ParametersAreNonnullByDefault
public class FigureFactory {
    private static final Map<FigureType, FigureSupplier> FIGURE_SUPPLIERS_MAP = ImmutableMap.of(
            FigureType.CIRCLE, new CircleSupplier(),
            FigureType.RECTANGLE, new RectangleSupplier(),
            FigureType.TRIANGLE, new TriangleSupplier()
    );

    /**
     * Поставляет фигуру.
     *
     * @param figureType тип возвращаемой фигуры.
     * @param parameters параметры возвращаемой фигуры.
     * @return фигура.
     * @throws NotCorrectDataException если {@code parameters} содержит неподходящие данной фигуре параметры.
     */
    @Nonnull
    public Figure getFigure(FigureType figureType, List<BigDecimal> parameters) throws NotCorrectDataException {

        return FIGURE_SUPPLIERS_MAP.get(figureType).getFigure(parameters);

    }
}
