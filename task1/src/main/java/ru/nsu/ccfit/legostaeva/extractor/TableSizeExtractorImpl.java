package ru.nsu.ccfit.legostaeva.extractor;

import ru.nsu.ccfit.legostaeva.validator.TableSizeValidator;
import ru.nsu.ccfit.legostaeva.validator.Validator;

import javax.annotation.Nullable;
import javax.annotation.WillNotClose;
import java.io.InputStream;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class TableSizeExtractorImpl implements TableSizeExtractor {
    private final Validator<Integer> validator = new TableSizeValidator();

    /**
     * {@inheritDoc}
     */
    @Override
    @Nullable
    public Integer getTableSizeFromArguments(String[] args) {
        if (args.length == 1) {
            try {
                int tableSize = Integer.parseInt(args[0]);
                if (validator.isValid(tableSize)) {
                    return tableSize;
                } else {
                    return null;
                }
            } catch (NumberFormatException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getTableSizeFromInputStream(@WillNotClose InputStream inputStream) {
        int tableSize = 0;
        Scanner in = new Scanner(inputStream);
        do {
            System.out.println("Enter the table size(from 1 to 32), please.");
            try {
                tableSize = in.nextInt();
            } catch (NoSuchElementException e) {
                in.next();
            }
        } while (!validator.isValid(tableSize));
        return tableSize;
    }
}