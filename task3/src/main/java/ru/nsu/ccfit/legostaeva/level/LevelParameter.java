package ru.nsu.ccfit.legostaeva.level;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Параметры уровня игры "Сапер":
 * <ul>
 * <li>Количество строк поля.</li>
 * <li>Количество столбцов поля.</li>
 * <li>Количество мин на поле.</li>
 * </ul>
 */
@Getter
@RequiredArgsConstructor
public class LevelParameter {
    private final int rowNumber;
    private final int columnNumber;
    private final int minesNumber;
}
