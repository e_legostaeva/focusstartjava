package ru.nsu.ccfit.legostaeva.task;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@RequiredArgsConstructor
public class Task {
    private final int lowerBoundOfDataInclusive;
    private final int upperBoundOfDataExclusive;
    private double result;
}