package ru.nsu.ccfit.legostaeva.model;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import ru.nsu.ccfit.legostaeva.exception.MinesweeperException;
import ru.nsu.ccfit.legostaeva.highscores.HighScoreManager;
import ru.nsu.ccfit.legostaeva.level.GameLevel;
import ru.nsu.ccfit.legostaeva.level.GameLevelUtils;
import ru.nsu.ccfit.legostaeva.level.LevelParameter;
import ru.nsu.ccfit.legostaeva.model.notifier.MinesweeperViewNotifier;
import ru.nsu.ccfit.legostaeva.model.notifier.ViewNotifier;
import ru.nsu.ccfit.legostaeva.view.MinesweeperView;

import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Модель - предоставляет данные и реагирует на команды {@link ru.nsu.ccfit.legostaeva.controller.MinesweeperController},
 * изменяя своё состояние.
 */
@Slf4j
@Setter
@Getter
public class MinesweeperModel {
    private static final int MINIMUM_AMOUNT_MINE_FREE_CELLS = 1;
    private static final int MINIMUM_ROWS_AMOUNT = 2;
    private static final int MINIMUM_COLUMN_AMOUNT = 2;
    private static final int MINIMUM_MINES_AMOUNT = 1;

    private final ViewNotifier minesweeperViewNotifier;
    private final HighScoreManager highscoreManager;

    private Field field;
    private Timer timer;
    private GameLevel gameLevel;
    private LevelParameter levelParameter;

    private int currentPlayTime;
    private int rowNumber;
    private int columnNumber;
    private int minesNumber;
    private int flaggedCellsNumber;
    private int maxFieldRows;
    private int maxFieldColumns;
    private int maxMinesAmount;
    private int maxMinesAmountForCurrentLevel;

    private boolean isGameOver;
    private boolean isFirstClick;

    public MinesweeperModel() {
        this.minesweeperViewNotifier = new MinesweeperViewNotifier();
        this.highscoreManager = new HighScoreManager();
        this.gameLevel = GameLevel.BEGINNER;
        this.levelParameter = GameLevelUtils.getLevelParametersByLevel(GameLevel.BEGINNER);
        this.rowNumber = levelParameter.getRowNumber();
        this.columnNumber = levelParameter.getColumnNumber();
        this.minesNumber = levelParameter.getMinesNumber();
        countMaxMinesAmountForCurrentLevel();
        startNewGame();
    }

    public void attachView(MinesweeperView minesweeperView) {
        minesweeperViewNotifier.attachView(minesweeperView);
    }

    public void changeLevelParameter(LevelParameter newLevelParameter) {
        this.rowNumber = newLevelParameter.getRowNumber();
        this.columnNumber = newLevelParameter.getColumnNumber();
        this.minesNumber = newLevelParameter.getMinesNumber();
    }

    public void startNewGame() {
        this.currentPlayTime = 0;
        this.field = new Field(this.rowNumber, this.columnNumber, this.minesNumber);
        this.isFirstClick = true;
        this.flaggedCellsNumber = 0;
        this.isGameOver = false;

        minesweeperViewNotifier.notifyViewsNewGame(this.rowNumber, this.columnNumber, this.minesNumber);
    }

    public void restartNewGame() {
        Optional.ofNullable(timer).ifPresent(Timer::cancel);

        if (!checkIfValuesLessThanMin()) {
            minesweeperViewNotifier.notifyViewAboutLittleGameParameters(MINIMUM_ROWS_AMOUNT, MINIMUM_COLUMN_AMOUNT, MINIMUM_MINES_AMOUNT);
            return;
        }

        if (!checkIfValuesGreaterThanMax()) {
            minesweeperViewNotifier.notifyViewAboutTooBigGameParameters(maxFieldRows, maxFieldColumns, maxMinesAmount);
            return;
        }

        if (minesNumber > maxMinesAmountForCurrentLevel) {
            minesweeperViewNotifier.notifyViewAboutTooBigMinesAmount(maxMinesAmountForCurrentLevel);
            return;
        }
        startNewGame();
    }

    public void addNewScoreToHighScores(String playerName) {
        try {
            highscoreManager.addScore(playerName, currentPlayTime, gameLevel);
        } catch (MinesweeperException e) {
            log.error("Произошла ошибка во время работы с файлом рекордов.", e);
            minesweeperViewNotifier.notifyViewAboutFailWhileAddingNewScore();
        }
    }

    public void showHighScores(GameLevel highScoresGameLevel) {
        String currentScores;
        try {
            currentScores = highscoreManager.getHighScoreString(highScoresGameLevel);
        } catch (MinesweeperException e) {
            log.error("Произошла ошибка во время работы с файлом рекордов.", e);
            minesweeperViewNotifier.notifyViewAboutFailWhileLoadingScores();
            return;
        }
        minesweeperViewNotifier.notifyViewAboutShowScores(currentScores);
    }

    public void openCell(int row, int column) {
        if (isGameOver || !field.checkCellExist(row, column)) {
            return;
        }

        Cell currentCell = new Cell(row, column);

        if (isFirstClick) {
            processFirstClick(currentCell);
        }

        currentCell = field.fetchCellByCoords(row, column);

        if (currentCell.getCurrentCellState() == CellState.CLOSED) {
            currentCell.setCurrentCellState(CellState.OPENED);
            if (gameIsLost(currentCell)) {
                endTheLostGame();
                return;
            }
            if (gameIsWon()) {
                endTheWinGame();
                return;
            }
            minesweeperViewNotifier.notifyViewsNewCellChanged(currentCell.getRow(),
                    currentCell.getColumn(),
                    generateCurrentCellType(currentCell)
            );

            if ((currentCell.getNumberOfMinesAround() == 0) && (!currentCell.isMined())) {
                openNeighbours(row, column);
            }
        }
    }

    private boolean gameIsWon() {
        return field.checkAllMinesAreFlagged() || field.checkAllMineFreeCellsAreOpened();
    }

    private void endTheWinGame() {
        timer.cancel();
        isGameOver = true;
        minesweeperViewNotifier.notifyViewsAboutGameWon(gameLevel);
    }

    private boolean gameIsLost(Cell cell) {
        return cell.isMined();
    }

    private void endTheLostGame() {
        timer.cancel();
        isGameOver = true;
        minesweeperViewNotifier.notifyViewsAboutGameOver(generateFieldCellsTypesAfterLose());
    }

    private void processFirstClick(Cell cell) {
        field.initializeField(cell);
        isFirstClick = false;
        timer = new Timer();
        timer.scheduleAtFixedRate(
                new TimerTask() {
                    public void run() {
                        currentPlayTime++;
                        minesweeperViewNotifier.notifyViewAboutTimeChange(currentPlayTime);
                    }
                },
                0,
                1000);
    }

    private void openNeighbours(int row, int column) {
        for (int i = row - 1; i <= row + 1; ++i) {
            for (int j = column - 1; j <= column + 1; ++j) {
                if (i != row || j != column) {
                    openCell(i, j);
                }
            }
        }
    }

    public void tryToOpenNeighbours(int row, int column) {
        Cell cell = field.fetchCellByCoords(row, column);

        if (cell.getCurrentCellState() != CellState.OPENED) {
            return;
        }

        int numberOfFlagsAround = 0;
        for (int i = row - 1; i <= row + 1; ++i) {
            for (int j = column - 1; j <= column + 1; ++j) {
                if (!field.checkCellExist(i, j)) {
                    continue;
                }
                Cell neighbour = field.fetchCellByCoords(i, j);
                if (neighbour.getCurrentCellState() == CellState.FLAGGED) {
                    numberOfFlagsAround++;
                }
            }
        }

        if (cell.getNumberOfMinesAround() != numberOfFlagsAround) {
            return;
        }

        for (int i = row - 1; i <= row + 1; ++i) {
            for (int j = column - 1; j <= column + 1; ++j) {
                if (!field.checkCellExist(i, j)) {
                    continue;
                }
                Cell neighbour = field.fetchCellByCoords(i, j);
                if (neighbour.getCurrentCellState() == CellState.FLAGGED && !neighbour.isMined() ||
                        neighbour.getCurrentCellState() != CellState.FLAGGED && neighbour.isMined()) {
                    endTheLostGame();
                    return;
                }

                if (!neighbour.isMined()) {
                    openCell(i, j);
                }

            }
        }
    }

    public void changeCellState(int row, int column) {
        Cell currentCell = field.fetchCellByCoords(row, column);

        switch (currentCell.getCurrentCellState()) {
            case CLOSED:
                putFlagOnCell(currentCell);
                break;
            case FLAGGED:
                putQuestionOnCell(currentCell);
                break;
            case QUESTION:
                makeCellClose(currentCell);
                break;
            case OPENED:
                break;
        }
    }

    private void putFlagOnCell(Cell cell) {
        if (flaggedCellsNumber == minesNumber) {
            return;
        }

        cell.setCurrentCellState(CellState.FLAGGED);
        flaggedCellsNumber++;
        minesweeperViewNotifier.notifyViewsNewCellChanged(cell.getRow(),
                cell.getColumn(),
                generateCurrentCellType(cell)
        );
        minesweeperViewNotifier.notifyViewAboutFlaggedCellNumber(flaggedCellsNumber, minesNumber);
        if (gameIsWon()) {
            endTheWinGame();
        }
    }

    private void putQuestionOnCell(Cell cell) {
        cell.setCurrentCellState(CellState.QUESTION);
        flaggedCellsNumber--;
        minesweeperViewNotifier.notifyViewsNewCellChanged(cell.getRow(),
                cell.getColumn(),
                generateCurrentCellType(cell)
        );
        minesweeperViewNotifier.notifyViewAboutFlaggedCellNumber(flaggedCellsNumber, minesNumber);
    }

    private void makeCellClose(Cell cell) {
        cell.setCurrentCellState(CellState.CLOSED);
        minesweeperViewNotifier.notifyViewsNewCellChanged(cell.getRow(),
                cell.getColumn(),
                generateCurrentCellType(cell)
        );
    }

    public void countMaxMinesAmount() {
        maxMinesAmount = maxFieldRows * maxFieldColumns - MINIMUM_AMOUNT_MINE_FREE_CELLS;
    }

    public void countMaxMinesAmountForCurrentLevel() {
        maxMinesAmountForCurrentLevel = rowNumber * columnNumber - MINIMUM_AMOUNT_MINE_FREE_CELLS;
    }

    private boolean checkIfValuesGreaterThanMax() {
        return (rowNumber <= maxFieldRows
                && columnNumber <= maxFieldColumns
                && minesNumber <= maxMinesAmount
        );
    }

    private boolean checkIfValuesLessThanMin() {
        return (rowNumber >= MINIMUM_ROWS_AMOUNT
                && columnNumber >= MINIMUM_COLUMN_AMOUNT
                && minesNumber >= MINIMUM_MINES_AMOUNT
        );
    }

    private String generateCurrentCellType(Cell cell) {
        return cell.getCurrentCellState().getCellType(cell);
    }

    private String[][] generateFieldCellsTypesAfterLose() {
        String[][] fieldTypes = new String[rowNumber][columnNumber];
        for (int i = 0; i < rowNumber; ++i) {
            for (int j = 0; j < columnNumber; ++j) {
                fieldTypes[i][j] = generateCellTypeAfterLose(field.fetchCellByCoords(i, j));
            }
        }
        return fieldTypes;
    }

    private String generateCellTypeAfterLose(Cell cell) {
        return cell.getCurrentCellState().getCellTypeAfterLose(cell);
    }
}
