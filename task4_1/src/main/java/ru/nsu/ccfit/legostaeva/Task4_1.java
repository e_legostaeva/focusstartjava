package ru.nsu.ccfit.legostaeva;

import lombok.extern.slf4j.Slf4j;
import ru.nsu.ccfit.legostaeva.extractor.UpperExtractorImpl;
import ru.nsu.ccfit.legostaeva.extractor.UpperLimitExtractor;
import ru.nsu.ccfit.legostaeva.task.Task;
import ru.nsu.ccfit.legostaeva.task.TaskProducer;
import ru.nsu.ccfit.legostaeva.task.WorkQueue;
import ru.nsu.ccfit.legostaeva.task.handler.TaskHandler;

import java.util.List;
import java.util.Optional;

@Slf4j
public class Task4_1 {
    private static final int NANOSEC_TO_MSEC = 1000000;
    private static final int THREADS_NUMBER = 10;

    public static void main(String[] args) {
        UpperLimitExtractor upperLimitExtractor = new UpperExtractorImpl();

        int upperLimit;

        try {
            upperLimit = Optional.ofNullable(upperLimitExtractor.getUpperLimitFromArguments(args))
                    .orElseGet(() -> upperLimitExtractor.getUpperLimitFromInputStream(System.in));
        } catch (IllegalStateException e) {
            log.error("Error while reading from input");
            return;
        }

        TaskProducer taskProducer = new TaskProducer(upperLimit);
        List<Task> tasks = taskProducer.produce();

        int appropriateThreadsNumber = THREADS_NUMBER;

        if (tasks.size() < appropriateThreadsNumber) {
            appropriateThreadsNumber = tasks.size();
        }

        WorkQueue workQueue = new WorkQueue(appropriateThreadsNumber);

        long startTime = System.nanoTime();

        tasks.stream()
                .map(TaskHandler::new)
                .forEach(workQueue::execute);

        workQueue.terminateExecutionOfAllThreads();

        double endTime = (double) (System.nanoTime() - startTime) / NANOSEC_TO_MSEC;

        log.info("work time = " + endTime + " ms");

        double result = tasks.stream()
                .map(Task::getResult)
                .reduce(0D, Double::sum);

        System.out.println("result = " + result);

    }
}
