package ru.nsu.ccfit.legostaeva.model.notifier;

import ru.nsu.ccfit.legostaeva.level.GameLevel;
import ru.nsu.ccfit.legostaeva.view.MinesweeperView;

import java.util.ArrayList;
import java.util.List;

/**
 * {@inheritDoc}
 */
public class MinesweeperViewNotifier implements ViewNotifier {
    private final List<MinesweeperView> minesweeperViews = new ArrayList<>();

    @Override
    public void attachView(MinesweeperView minesweeperView) {
        minesweeperViews.add(minesweeperView);
    }

    @Override
    public void notifyViewsNewGame(int rowNumber, int columnNumber, int minesNumber) {
        minesweeperViews.forEach(minesweeperView -> minesweeperView.renderNewGame(rowNumber, columnNumber, minesNumber));
    }

    @Override
    public void notifyViewsNewCellChanged(int row, int column, String cellType) {
        minesweeperViews.forEach(minesweeperView -> minesweeperView.updateCell(row, column, cellType));
    }

    @Override
    public void notifyViewsAboutGameOver(String[][] fieldTypes) {
        minesweeperViews.forEach(minesweeperView -> minesweeperView.renderGameLost(fieldTypes));
    }

    @Override
    public void notifyViewsAboutGameWon(GameLevel gameLevel) {
        minesweeperViews.forEach(minesweeperView -> minesweeperView.renderGameWon(gameLevel));
    }

    @Override
    public void notifyViewAboutFlaggedCellNumber(int flaggedCellNumber, int minesNumber) {
        minesweeperViews.forEach(minesweeperView -> minesweeperView.updateMinesCounter(flaggedCellNumber, minesNumber));
    }

    @Override
    public void notifyViewAboutTimeChange(int timeSeconds) {
        minesweeperViews.forEach(minesweeperView -> minesweeperView.updateTimer(timeSeconds));
    }

    @Override
    public void notifyViewAboutFailWhileAddingNewScore() {
        minesweeperViews.forEach(MinesweeperView::renderFailWhileAddingNewScore);
    }

    @Override
    public void notifyViewAboutFailWhileLoadingScores() {
        minesweeperViews.forEach(MinesweeperView::renderFailWhileLoadingScores);
    }

    @Override
    public void notifyViewAboutShowScores(String scores) {
        minesweeperViews.forEach(minesweeperView -> minesweeperView.showHighScores(scores));
    }

    @Override
    public void notifyViewAboutTooBigGameParameters(int maxRows, int maxColumns, int maxMinesAmount) {
        minesweeperViews.forEach(minesweeperView -> minesweeperView.renderTooBigGameParameters(maxRows, maxColumns, maxMinesAmount));
    }

    @Override
    public void notifyViewAboutLittleGameParameters(int minRows, int minColumns, int minMinesAmount) {
        minesweeperViews.forEach(minesweeperView -> minesweeperView.renderLittleGameParameters(minRows, minColumns, minMinesAmount));
    }

    @Override
    public void notifyViewAboutTooBigMinesAmount(int maxMinesAmount) {
        minesweeperViews.forEach(minesweeperView -> minesweeperView.renderTooBigMinesAmount(maxMinesAmount));
    }
}
