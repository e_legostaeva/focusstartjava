package ru.nsu.ccfit.legostaeva.figure.implementations;

import lombok.ToString;
import ru.nsu.ccfit.legostaeva.exception.NotCorrectDataException;
import ru.nsu.ccfit.legostaeva.figure.Figure;
import ru.nsu.ccfit.legostaeva.figure.FigureType;
import ru.nsu.ccfit.legostaeva.figure.ParameterValue;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static ru.nsu.ccfit.legostaeva.figure.Parameter.DIAMETER;
import static ru.nsu.ccfit.legostaeva.figure.Parameter.RADIUS;

@ToString
@ParametersAreNonnullByDefault
public class Circle extends Figure {
    private final BigDecimal radius;

    public Circle(List<BigDecimal> paramsList) throws NotCorrectDataException {
        validateParametersNumber(paramsList);
        this.radius = paramsList.get(0).setScale(2, RoundingMode.HALF_UP);
    }

    @Nonnull
    @Override
    public List<ParameterValue> getParameters() {
        return getDefaultProperties()
                .add(new ParameterValue(RADIUS, radius.toString()))
                .add(new ParameterValue(DIAMETER, getDiameter().toString()))
                .build();
    }

    @Nonnull
    private BigDecimal getDiameter() {
        return radius
                .multiply(BigDecimal.valueOf(2))
                .setScale(2, RoundingMode.HALF_UP);
    }

    @Nonnull
    @Override
    protected BigDecimal getPerimeter() {
        return getDiameter()
                .multiply(BigDecimal.valueOf(Math.PI))
                .setScale(2, RoundingMode.HALF_UP);
    }

    @Nonnull
    @Override
    protected FigureType getFigureType() {
        return FigureType.CIRCLE;
    }

    @Nonnull
    @Override
    protected BigDecimal getArea() {
        return radius
                .multiply(radius)
                .multiply(BigDecimal.valueOf(Math.PI))
                .setScale(2, RoundingMode.HALF_UP);
    }

    @Nonnull
    @Override
    protected Long getParametersNumber() {
        return 1L;
    }
}
