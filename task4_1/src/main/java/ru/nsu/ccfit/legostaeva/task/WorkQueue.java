package ru.nsu.ccfit.legostaeva.task;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedList;

/**
 * ThreadPool, который обрабатывает задачи.
 */
@Slf4j
public class WorkQueue {
    private final PoolWorker[] threads;
    private final LinkedList<Runnable> queue;
    private volatile boolean isTimeToFinish;

    public WorkQueue(int threadsNumber) {
        queue = new LinkedList<>();
        threads = new PoolWorker[threadsNumber];

        for (int i = 0; i < threadsNumber; i++) {
            threads[i] = new PoolWorker(i);
            threads[i].start();
            log.info("Thread number {} is ready for work.", i);
        }
    }

    /**
     * Добавляет задачи в очередь на исполнение, а также оповещает об этом все доступные потоки.
     *
     * @param command задача, требующая выполнения.
     */
    public void execute(Runnable command) {
        synchronized (queue) {
            queue.addLast(command);
            queue.notifyAll();
        }
    }

    /**
     * Оповещает ждущие потоки об окончании работы, ждет их завершение.
     */
    public void terminateExecutionOfAllThreads() {
        isTimeToFinish = true;
        synchronized (queue) {
            queue.notifyAll();
        }

        for (PoolWorker poolWorker : threads) {
            try {
                poolWorker.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Поток-обработчик задачи
     */
    @RequiredArgsConstructor
    private class PoolWorker extends Thread {
        private final int indexOfCurrentThread;

        public void run() {
            Runnable command;
            while (true) {
                synchronized (queue) {
                    while (queue.isEmpty()) {
                        try {
                            if (isTimeToFinish) {
                                log.info("For thread number {} is time to finish.", indexOfCurrentThread);
                                return;
                            }
                            queue.wait();
                            log.info("Thread number {} is waiting.", indexOfCurrentThread);
                        } catch (InterruptedException ignored) {
                            return;
                        }
                    }
                    command = queue.removeFirst();
                }

                try {
                    command.run();
                    log.info("Thread number {} is running command {}.", indexOfCurrentThread, command);
                } catch (RuntimeException e) {
                    log.error("Uncaught exception.", e);
                }
            }
        }
    }
}