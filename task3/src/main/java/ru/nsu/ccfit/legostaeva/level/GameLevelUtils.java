package ru.nsu.ccfit.legostaeva.level;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * Класс, сопоставляющий {@link GameLevel} со значениями {@link LevelParameter} по умолчанию.
 */
public final class GameLevelUtils {
    private static final Map<GameLevel, LevelParameter> LEVEL_SETTINGS = ImmutableMap.of(
            GameLevel.BEGINNER, new LevelParameter(9, 9, 10),
            GameLevel.INTERMEDIATE, new LevelParameter(16, 16, 40),
            GameLevel.EXPERT, new LevelParameter(16, 30, 99)
    );

    public static LevelParameter getLevelParametersByLevel(GameLevel gameLevel) {
        return LEVEL_SETTINGS.get(gameLevel);
    }
}
