package ru.nsu.ccfit.legostaeva.controller;

import ru.nsu.ccfit.legostaeva.level.GameLevel;
import ru.nsu.ccfit.legostaeva.level.LevelParameter;
import ru.nsu.ccfit.legostaeva.model.MinesweeperModel;

/**
 * Контроллер - интерпретирует действия пользователя в {@link ru.nsu.ccfit.legostaeva.view.MinesweeperView},
 * оповещая {@link MinesweeperModel} о необходимости изменений.
 */
public class MinesweeperController {
    private final MinesweeperModel model;

    public MinesweeperController(MinesweeperModel minesweeperModel) {
        this.model = minesweeperModel;
    }

    public void startNewGameHandle() {
        model.startNewGame();
    }

    public void restartNewGameHandle() {
        model.restartNewGame();
    }

    public void changeLevelParameterHandle(LevelParameter levelParameter) {
        model.changeLevelParameter(levelParameter);
        model.countMaxMinesAmountForCurrentLevel();
    }

    public void changeLevelHandle(GameLevel newLevel) {
        model.setGameLevel(newLevel);
    }

    public void leftClickHandle(int row, int column) {
        model.openCell(row, column);
    }

    public void rightClickHandle(int row, int column) {
        model.changeCellState(row, column);
    }

    public void middleClickHandle(int row, int column) {
        model.tryToOpenNeighbours(row, column);
    }

    public void highScoreNewValueHandle(String playerName) {
        model.addNewScoreToHighScores(playerName);
    }

    public void showHighScoresHandle(GameLevel gameLevel) {
        model.showHighScores(gameLevel);
    }

    public void maxFieldSizeHandle(int maxHeight, int maxWidth) {
        model.setMaxFieldRows(maxHeight);
        model.setMaxFieldColumns(maxWidth);
        model.countMaxMinesAmount();
    }
}
