package ru.nsu.ccfit.legostaeva.level;

/**
 * Уровни игры "Сапер".
 */
public enum GameLevel {
    BEGINNER,
    INTERMEDIATE,
    EXPERT,
    SPECIAL
}
