package ru.nsu.ccfit.legostaeva.highscores;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

/**
 * Тип записи рекордов в файлы, используемые {@link HighScoreManager}.
 */
@Getter
@RequiredArgsConstructor
class Score implements Serializable {
    private final String name;
    private final int score;
}
