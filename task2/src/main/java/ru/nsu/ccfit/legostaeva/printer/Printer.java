package ru.nsu.ccfit.legostaeva.printer;

import ru.nsu.ccfit.legostaeva.figure.ParameterValue;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.WillNotClose;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

@ParametersAreNonnullByDefault
public interface Printer {
    /**
     * Выводит список указанных параметров в указанный поток.
     *
     * @param parameters   список параметров, которые необходимо вывести.
     * @param outputStream поток для записи выходных данных.
     */
    void printParameters(List<ParameterValue> parameters, @WillNotClose OutputStream outputStream) throws IOException;
}
