package ru.nsu.ccfit.legostaeva.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Игровая ячейка.
 */
@Getter
@Setter
class Cell {
    private final int row;
    private final int column;
    private CellState currentCellState;
    private boolean isMined;
    private int numberOfMinesAround;

    Cell(int row, int column) {
        this.row = row;
        this.column = column;
        this.currentCellState = CellState.CLOSED;
    }

    void increaseNumberOfMinesAround() {
        this.numberOfMinesAround++;
    }
}