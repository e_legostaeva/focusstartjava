package ru.nsu.ccfit.legostaeva;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.UUID;
import java.util.concurrent.BlockingQueue;

@Slf4j
@RequiredArgsConstructor
public class Producer implements Runnable {
    private final BlockingQueue<Resource> sharedQueue;

    @Override
    public void run() {
        UUID uuid = UUID.randomUUID();
        Resource resource = new Resource(uuid);
        try {
            sharedQueue.put(resource);
            log.info("Thread type: producer, Resource ID: {}, Produced", uuid);
        } catch (ClassCastException | IllegalArgumentException | InterruptedException e) {
            log.warn("Возникли проблемы при попытке добавить ресурс в очередь", e);
        }
    }
}
