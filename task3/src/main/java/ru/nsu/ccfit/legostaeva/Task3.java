package ru.nsu.ccfit.legostaeva;

import ru.nsu.ccfit.legostaeva.controller.MinesweeperController;
import ru.nsu.ccfit.legostaeva.model.MinesweeperModel;
import ru.nsu.ccfit.legostaeva.view.MinesweeperView;
import ru.nsu.ccfit.legostaeva.view.SwingMinesweeperView;

public class Task3 {
    public static void main(String[] args) {
        MinesweeperModel minesweeperModel = new MinesweeperModel();
        MinesweeperController minesweeperController = new MinesweeperController(minesweeperModel);
        MinesweeperView minesweeperView = new SwingMinesweeperView(minesweeperController);

        minesweeperModel.attachView(minesweeperView);
    }
}
