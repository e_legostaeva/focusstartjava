package ru.nsu.ccfit.legostaeva.validator;

public class TableSizeValidator implements Validator<Integer> {
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isValid(Integer tableSize) {
        return (tableSize <= 32) && (tableSize >= 1);
    }
}