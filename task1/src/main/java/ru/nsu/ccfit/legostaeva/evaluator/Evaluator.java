package ru.nsu.ccfit.legostaeva.evaluator;

public interface Evaluator {
    /**
     * Применить заданную операцию к числам от 1 до size
     *
     * @param size размер желаемой таблицы
     * @return массив int[][], элементы которого являются результатом выполненной операции для каждого числа
     */
    int[][] evaluate(int size);
}