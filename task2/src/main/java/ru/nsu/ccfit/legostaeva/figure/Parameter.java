package ru.nsu.ccfit.legostaeva.figure;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Параметры всех используемых фигур.
 */
@Getter
@RequiredArgsConstructor
public enum Parameter {
    NAME("Тип фигуры", ""),
    AREA("Площадь", "кв. мм"),
    PERIMETER("Периметр", "мм"),
    RADIUS("Радиус", "мм"),
    DIAMETER("Диаметр", "мм"),
    DIAGONAL_LENGTH("Длина диагонали", "мм"),
    LENGTH("Длина", "мм"),
    WIDTH("Ширина", "мм"),
    SIDE_LENGTH("Длина стороны", "мм"),
    OPPOSITE_ANGLE("Противолежащий угол", "гр.");

    private final String readableName;
    private final String unit;
}
