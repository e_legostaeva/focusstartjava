package ru.nsu.ccfit.legostaeva.valueloader;

import lombok.extern.slf4j.Slf4j;
import ru.nsu.ccfit.legostaeva.exception.NotCorrectDataException;
import ru.nsu.ccfit.legostaeva.validator.PropertiesValueValidator;

import javax.annotation.Nonnull;
import javax.annotation.WillNotClose;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Класс-загрузчик данных из файлов ".properties"
 */
@Slf4j
public class PropertiesValueLoader {
    private static final String PRODUCER_THREADS_NUMBER = "N";
    private static final String CONSUMER_THREADS_NUMBER = "M";
    private static final String PRODUCER_THREADS_TIME = "tN";
    private static final String CONSUMER_THREADS_TIME = "tM";
    private static final String STORAGE_SIZE = "S";

    private final PropertiesValueValidator propertiesValueValidator = new PropertiesValueValidator();

    /**
     * Получает параметры, необходимые для работы программы, из входного потока данных.
     *
     * @param inputStream входной поток данных.
     * @return {@link PropertiesValueLoaderResult}, в котором лежат полученные параметры.
     * @throws IOException             если произошла ошибка при чтении из .properties.
     * @throws NotCorrectDataException если в .properties найдены некорректные данные, либо сам файл не сушествует/нет
     *                                 прав на чтение.
     */
    @Nonnull
    public PropertiesValueLoaderResult loadPropertiesValues(@WillNotClose InputStream inputStream) throws NotCorrectDataException, IOException {
        Properties appProperties = new Properties();
        try {
            appProperties.load(inputStream);
        } catch (IllegalArgumentException e) {
            throw new NotCorrectDataException("Ошибка во время загрузки данных из .properties");
        }

        for (String key : appProperties.stringPropertyNames()) {
            try {
                if (!propertiesValueValidator.isValid(Integer.parseInt(appProperties.getProperty(key)))) {
                    throw new NotCorrectDataException("Отрицательные числа в .properties");
                }
            } catch (NumberFormatException e) {
                throw new NotCorrectDataException("Отсутствует числовое значение в .properties");
            }
        }

        return new PropertiesValueLoaderResult()
                .setProducerThreadsNumber(Integer.parseInt(appProperties.getProperty(PRODUCER_THREADS_NUMBER)))
                .setConsumerThreadsNumber(Integer.parseInt(appProperties.getProperty(CONSUMER_THREADS_NUMBER)))
                .setTimeForProducerThreads(Integer.parseInt(appProperties.getProperty(PRODUCER_THREADS_TIME)))
                .setTimeForConsumerThreads(Integer.parseInt(appProperties.getProperty(CONSUMER_THREADS_TIME)))
                .setStorageSize(Integer.parseInt(appProperties.getProperty(STORAGE_SIZE)));
    }
}
