package ru.nsu.ccfit.legostaeva.figure.implementations;

import lombok.ToString;
import ru.nsu.ccfit.legostaeva.figure.Figure;
import ru.nsu.ccfit.legostaeva.figure.FigureType;
import ru.nsu.ccfit.legostaeva.figure.ParameterValue;
import ru.nsu.ccfit.legostaeva.exception.NotCorrectDataException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static ru.nsu.ccfit.legostaeva.figure.Parameter.*;

@ToString
@ParametersAreNonnullByDefault
public class Rectangle extends Figure {
    private final BigDecimal length;
    private final BigDecimal width;

    @SuppressWarnings("OptionalGetWithoutIsPresent")
    public Rectangle(List<BigDecimal> parameters) throws NotCorrectDataException {
        validateParametersNumber(parameters);
        length = parameters.stream()
                .max(BigDecimal::compareTo)
                .get()
                .setScale(2, RoundingMode.HALF_UP);
        width = parameters.stream()
                .min(BigDecimal::compareTo)
                .get()
                .setScale(2, RoundingMode.HALF_UP);
    }

    @Nonnull
    @Override
    public List<ParameterValue> getParameters() {
        return getDefaultProperties()
                .add(new ParameterValue(DIAGONAL_LENGTH, getDiagonalLength().toString()))
                .add(new ParameterValue(LENGTH, length.toString()))
                .add(new ParameterValue(WIDTH, width.toString()))
                .build();
    }

    @Nonnull
    @Override
    protected FigureType getFigureType() {
        return FigureType.RECTANGLE;
    }

    @Nonnull
    @Override
    protected BigDecimal getArea() {
        return length
                .multiply(width)
                .setScale(2, RoundingMode.HALF_UP);
    }

    @Nonnull
    @Override
    protected BigDecimal getPerimeter() {
        return length
                .add(width)
                .multiply(BigDecimal.valueOf(2))
                .setScale(2, RoundingMode.HALF_UP);
    }

    @Nonnull
    @Override
    protected Long getParametersNumber() {
        return 2L;
    }

    @Nonnull
    private BigDecimal getDiagonalLength() {
        return BigDecimal
                .valueOf(Math.sqrt(length
                        .multiply(length)
                        .add(width.multiply(width))
                        .doubleValue()))
                .setScale(2, RoundingMode.HALF_UP);
    }
}
