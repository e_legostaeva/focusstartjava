package ru.nsu.ccfit.legostaeva.printer;

import ru.nsu.ccfit.legostaeva.figure.ParameterValue;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.WillNotClose;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

@ParametersAreNonnullByDefault
public class PrinterImpl implements Printer {

    /**
     * {@inheritDoc}
     */
    @Override
    public void printParameters(List<ParameterValue> parameters, @WillNotClose OutputStream outputStream) throws IOException {
        for (ParameterValue parameterValue : parameters) {
            outputStream.write(
                    String.format(
                            "%s: %s %s" + System.lineSeparator(),
                            parameterValue.getParameter().getReadableName(),
                            parameterValue.getValue(),
                            parameterValue.getParameter().getUnit()
                    ).getBytes()
            );
        }
    }
}
