package ru.nsu.ccfit.legostaeva.parser.data;

import lombok.extern.slf4j.Slf4j;
import ru.nsu.ccfit.legostaeva.figure.FigureType;
import ru.nsu.ccfit.legostaeva.exception.NotCorrectDataException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.WillNotClose;
import java.io.InputStream;
import java.util.Scanner;

@Slf4j
@ParametersAreNonnullByDefault
public class DataParser {

    /**
     * Получает параметры, необходимые для работы программы, из входного потока данных.
     *
     * @param inputStream входной поток данных.
     * @return {@link DataParsingResult}, в котором лежат полученные параметры.
     * @throws NotCorrectDataException если тип фигуры отсутствует или некорректен, либо параметры фигуры отсутствуют.
     */
    @Nonnull
    public DataParsingResult parseArguments(@WillNotClose InputStream inputStream) throws NotCorrectDataException {
        DataParsingResult dataParsingResult = new DataParsingResult();

        Scanner dataScanner = new Scanner(inputStream);

        if (!dataScanner.hasNextLine()) {
            throw new NotCorrectDataException("Отсутствует тип фигуры.");
        }

        String inputFigureType = dataScanner.nextLine().toUpperCase();

        try {
            FigureType figureType = FigureType.valueOf(inputFigureType);
            dataParsingResult.setName(figureType);
        } catch (IllegalArgumentException e) {
            throw new NotCorrectDataException("Фигуры с типом " + inputFigureType + " не существует в программе.");
        }

        while (dataScanner.hasNextBigDecimal()) {
            dataParsingResult.addParameter(dataScanner.nextBigDecimal());
        }

        if (dataParsingResult.getParams().isEmpty()) {
            throw new NotCorrectDataException("Отсутсвуют параметры фигуры.");
        }

        log.debug("Результат разбора файла: {}", dataParsingResult);
        return dataParsingResult;
    }
}
